MagicStore
============
>Group: Luffandri (1901498321), Ramada Atyusa (1901499280), Steven Muliamin (1901498763), Thomas Dwi Dinata (1901498151)

------------

> Using [NetBeans 8.1][], [JDK 8][], and [MySQL 5.7][]

> The full release notes are available on **Release Notes.txt**

#### Java Application
> The goal of this project is to create a Java Application (with the concept of Object Oriented Programming) that handles data from a Database Management System Server (with relational database) that handles online game store.


#### Database Table
> Database Tables are available on MySQL Dump File

LATEST UPDATE : Version 0.1 BETA

[NetBeans 8.1]: <https://netbeans.org/>
[JDK 8]: <http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html>
[MySQL 5.7]: <http://dev.mysql.com/downloads/mysql/>