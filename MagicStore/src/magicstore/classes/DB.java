/*

Editor : Thomas Dwi Dinata

*/

package magicstore.classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public final class DB
{
    private static ArrayList<String> configuration;
    private static Connection connection = null;
    private static Statement st = null;

    public DB()
    {
        getConfig();
    }
    
    public static void getConfig() // Get Configuration from Config Class
    {
        configuration = ProgramConfig.getServerConfig();
    }
    
    public static boolean connectDefault() // Used for testing, connects to Server with the default configuration
    {
        return connect(configuration);
    }
    
    public static boolean testConnect(ArrayList<String> alist)// Used for testing submitted configuratons
    {
        return connect(alist);
    }
    
    public static boolean connect(ArrayList<String> config) // Connects to MySQL Server
    {
        int arrayIteration = 0;
        arrayIteration=0;
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + config.get(arrayIteration++) + "/" + config.get(arrayIteration++), config.get(arrayIteration++), config.get(arrayIteration++));
            st = connection.createStatement();
        }
        catch (ClassNotFoundException ex)
        {
            JOptionPane.showMessageDialog(null, "Driver not found, please contact the administrator! Error : "+ex, "Database Connection", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Can't connect to the Database! Error: " + ex, "Database Connection", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "An error has occured! Error: " + ex, "Database Connection", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
    
    public static void disconnect() // Disconnects from MySQL Server
    {
        System.out.println("DB.disconnect");
        try
        {    
            connection.close();
        }
        catch (SQLException ex)
        {
            JOptionPane.showConfirmDialog(null, "Cannot safely disconnect from Database Servr. Error: " + ex);
        }
    }
    
    public static ResultSet executeQuery(String query) // Execute any queries that will return a ResultSet
    {
        try
        {
            connect(configuration);
            ResultSet rs = st.executeQuery(query);
            return rs;
        }
        catch (SQLException ex)
        {
            showError(ex);
            return null;
        }
        catch(Exception e)
        {
            showError(e);
            return null;
        }
    }
    
    public static int executeUpdate(String query) // Execute SQL Queries that returns nothing or Integer
    {
        try
        {
            connect(configuration);
            int result = st.executeUpdate(query);
            disconnect();
            return result;
        }
        catch (SQLException ex)
        {
            showError(ex);
            return -1;
        }
        catch(Exception e)
        {
            showError(e);
            return -1;
        }
    }
    
    private static void showError(Exception ex) // Display an error while running SQL Query
    {
        JOptionPane.showMessageDialog(null, "An error occured while running SQL Query! Error : " + ex, "Query Processor", JOptionPane.ERROR_MESSAGE);
    }
}
