/*

Editor : Thomas Dwi Dinata

*/

package magicstore.classes;

import java.util.ArrayList;


public final class Config implements java.io.Serializable
{
    private static String hostname;
    private static String username;
    private static String password;
    private static String database;
    private static ArrayList<String> searchableItems;
    
    public static void setConfig(ArrayList<String> alist)
    {
        int i = 0; // ArrayList Index pointer
        hostname = alist.get(i++);
        database = alist.get(i++);
        username = alist.get(i++);
        password = alist.get(i++);
        searchableItems = new ArrayList<String>();
        searchableItems.add("Developer Name");
        searchableItems.add("Publisher Name");
        searchableItems.add("Tag");
        searchableItems.add("Platform");
    }
    
    public static void resetAlist(ArrayList<String> alist) // Re=setting the SearchableItems
    {
        searchableItems = alist;
    }
    
    public static String getHost() // Get hostname
    {
        return hostname;
    }
    
    public static String getUsername() // Get MySQL Server username
    {
        return username;
    }
    
    public static String getPassword() // Get MySQL Server password
    {
        return password;
    }
    
    public static String getDatabase() // Get Database name
    {
        return database;
    }
    
    public static ArrayList<String> getAllConfig()
    {
        ArrayList<String> alist = new ArrayList<String>();
        alist.add(hostname);
        alist.add(database);
        alist.add(username);
        alist.add(password);
        return alist;
    }

    public static ArrayList<String> getSearchItem() // returns Searchable Items for HomeScreen
    {
        return searchableItems;
    }
    
    public static void defaultConfig() // Set the default configuration to be used for DB
    {
        hostname = "localhost:3306";
        username = "root";
        password = "";
        database = "magicstore";
        searchableItems = new ArrayList<String>();
        searchableItems.add("Developer Name");
        searchableItems.add("Publisher Name");
        searchableItems.add("Tag");
    }
}
