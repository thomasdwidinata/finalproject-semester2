/*

Editor : Thomas Dwi Dinata

*/

package magicstore.classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class LoginCredentials
{
    private static String username;
    private static int userID;
    private static boolean LoginValid;
    
    public LoginCredentials()
    {
        dropLogin(); // Clearing memory
    }
    
    public static void dropLogin() // Used for Logging Out
    {
        LoginValid = false;
        username = "";
        userID = 0;
    }
    
    public static void setLogin(String uName, int id) // Used for Logging In
    {
        username = uName;
        userID = id;
        LoginValid = true;
    }
    
    public static String getLogin() // Retunrs logged in user name
    {
        if(LoginValid)
            return username;
        return null;
    }
    
    public static int getID() // Returns logged in user ID
    {
        if(LoginValid)
            return userID;
        return 0;
    }
    
    public static boolean isLoginValid() // Checks whether the login is valid or not
    {
        return LoginValid;
    }
    
    public static int[] checkLogin(ArrayList<String> data) // To detect whether current logged in user is a User or an Administrator
    {
        int[] idgroup = new int[2];
        idgroup[1] = 0;
        idgroup[0] = 0;
        ResultSet rs = QueryProcessor.getCustom(1, data);
        try{
        while(rs.next())
        {
            if(data.get(0).equals(rs.getString("username")))
            {
               if(data.get(1).equals(rs.getString("password")))
               {
                   setLogin(data.get(0), rs.getInt("user_id"));
                   
                   idgroup[0] = rs.getInt("user_id");
                   idgroup[1] = rs.getInt("permission_group");
                   return idgroup;
               }
            }
        }}catch(SQLException e){JOptionPane.showMessageDialog(null, "Data Unavailable! Error: " + e);}
        return idgroup;
    }
}