/*

Editor : Thomas Dwi Dinta

*/

package magicstore.classes;

import javax.swing.JFrame;

public final class GUIController
{
    private static JFrame jframe;
    public GUIController(JFrame newjframe)
    {
        jframe = newjframe;
    }
    public static void setEnabled(boolean bol) // To enable JFrame stored on `jframe`
    {
        jframe.setEnabled(bol);
        if(bol)
        {
            jframe.setVisible(bol);
        }
    }
    public static void setVisible(boolean bol) // To show JFrame stored on `jframe`
    {
        jframe.setVisible(bol);
    }
    public static void dispose() // To dispose JFame stores on `jframe`
    {
        jframe.dispose();
    }
    public static void recentre() // To recentre the JFrame position to the centre of the screen
    {
        jframe.setLocationRelativeTo(null);
    }
}
