/*

Editor : Thomas Dwi Dinata, Steven Muliamin, Luffandri, Ramada Atyusa

*/

package magicstore.classes;

import java.sql.ResultSet;
import java.util.ArrayList;

/*
    Common ArrayList Indexing
    
    Index 0     = Table Name
    Index 1     = Selected Field Name
    Index > 1   = Data;
    
    *** NOT ALL OF THESE STATEMENTS FOLLOW THIS RULE, ESPECIALLY WHEN USING MANY FIELDS ***
*/

public class QueryProcessor
{
    public static boolean defaultConnect() // Used for testing connection between Java JDBC Driver and MySQL Server
    {
        return DB.connectDefault();
    }
    
    public static boolean testConnect(ArrayList<String> alist)// Used for testing connection between Java JDBC Driver and MySQL Server
    {
        boolean bol = DB.testConnect(alist);
        DB.disconnect();
        return bol;
    }
    
    private static ArrayList<String> switcher(ArrayList<String> data) // To switch values for easy development, This is used to adapt current code to match with desired tables
    {
        ArrayList<String> alist = new ArrayList<String>();
        alist = data;
        for(int i = 0; i < alist.size() ; ++i)
            switch(alist.get(i))
            {
                case "Developer Name":
                    alist.set(i, "d.developer_name"); break;
                case "Publisher Name":
                    alist.set(i, "p.publisher_name"); break;
                case "Tag":
                    alist.set(i, "t.tag_name"); break;
                case "User Name":
                    alist.set(i, "u.username"); break;
                case "Game ID":
                    alist.set(i, "g.game_id"); break;
                case "Game Name":
                    alist.set(i, "g.game_name"); break;
                case "Price":
                    alist.set(i, "g.price"); break;
                case "Platform":
                    alist.set(i, "pl.platform_name"); break;
                default: break;
            }
        return alist;
    }
    
    public static ResultSet getCustom(int queryNo, ArrayList<String> data) // Custom Get, which able to receive many data. Usually refers to a specific data
    {
        int i = 0;
        data = switcher(data);
        switch(queryNo)
        {
            case 1: // Check Username and get Password
                return DB.executeQuery("SELECT u.username, u.password, u.user_id, u.permission_group FROM user u WHERE u.username = \"" + data.get(i++) + "\";");
            case 2: // Get checked items
                return DB.executeQuery("SELECT g.game_id AS `Game ID`, g.game_name AS `Game Name`, g.price AS `Price`, d.developer_name AS `Developer Name`, p.publisher_name AS `Publisher Name`, pl.platform_name AS `Platform`, t.tag_name AS `Tag` FROM game g, developer d, publisher p, tag t, platform_list pl WHERE g.developer_id = d.developer_id AND g.publisher_id = p.publisher_id AND g.tag_id = t.tag_id AND g.platform_id = pl.platform_id AND " + data.get(0) + " LIKE \"%"+ data.get(1) + "%\";");  
            case 3: // Get all games (Sort by ___)
                return DB.executeQuery("SELECT g.game_id AS `Game ID`, g.game_name AS `Game Name`, g.price AS `Price`, d.developer_name AS `Developer Name`, p.publisher_name AS `Publisher Name`, pl.platform_name AS `Platform`, t.tag_name AS `Tag` FROM game g, developer d, publisher p, tag t, platform_list pl WHERE g.developer_id = d.developer_id AND g.publisher_id = p.publisher_id AND g.tag_id = t.tag_id AND g.platform_id = pl.platform_id ORDER BY " + data.get(i++) + " " + data.get(i++) + ";");
            case 4: // Get user's Purchases
                return DB.executeQuery("SELECT gph.order_id, ph.payment_date, p.payment_type, s.shipping_mode, g.game_name, gph.qty, ph.total FROM game_purchase_history gph, purchase_history ph, payment p, shipping s, game g, user u WHERE gph.order_id=ph.order_id AND p.payment_id=ph.payment_id AND s.shipping_id=ph.shipping_id AND g.game_id=gph.game_id AND u.user_id=ph.user_id AND u.user_id="+LoginCredentials.getID());
            case 5: // Get user's cart
                return DB.executeQuery("SELECT g.game_id AS `Game ID`, g.game_name, g.price, d.developer_name, p.publisher_name, c.qty FROM cart c, game g, developer d, publisher p WHERE c.game_id = g.game_id AND c.user_id = "+data.get(i++)+" AND d.developer_id = g.developer_id AND p.publisher_id = g.publisher_id;");
            case 6: // Get user's wishlist
                return DB.executeQuery("SELECT g.game_id AS `Game ID`, g.game_name AS `Game Name`, d.developer_name AS `Developer Name`, p.publisher_name AS `Publisher Name` FROM wishlist w, game g, developer d, publisher p WHERE w.game_id = g.game_id AND w.user_id = "+data.get(0)+" AND g.developer_id = d.developer_id AND g.publisher_id = p.publisher_id;");
            case 7: // Get user's reviews
                return DB.executeQuery("SELECT r.review_content AS `Review Content`, g.game_name AS `Game Name`, g.game_id AS `Game ID` FROM review r, game g WHERE r.user_id = "+data.get(i++)+" AND r.game_id = g.game_id;");
            case 8: // Get specific game information
                return DB.executeQuery("SELECT * FROM `game` WHERE game_id = " + data.get(i++) + ";");
            case 9: // Get Game details
                return DB.executeQuery("SELECT g.game_id, g.game_name, g.price, g.stock, g.description, g.release_date, d.developer_name, d.developer_id, p.publisher_name, p.publisher_id, pl.platform_name, pl.platform_id, t.tag_name, t.tag_id FROM game g, developer d, publisher p, tag t, platform_list pl WHERE g.game_id = " + data.get(i++) + " AND d.developer_id = g.developer_id AND p.publisher_id = g.publisher_id AND g.platform_id = pl.platform_id AND g.tag_id = t.tag_id;");
            case 10: // Get Stock of specific Game
                return DB.executeQuery("SELECT qty, COUNT(*) AS 'mycount' FROM cart WHERE user_id = " + data.get(i++) + " AND game_id = " + data.get(i++) +";");
            case 11: // Get all data about User
                return DB.executeQuery("SELECT * FROM user WHERE user_id = "+data.get(i++)+";");
            case 12: // Get review based on game ID (Full)
                return DB.executeQuery("SELECT r.game_id, g.game_name, r.review_content, u.username FROM review r, user u, game g WHERE r.user_id = u.user_id AND g.game_id = " + data.get(i++));
            case 13: // Get review based on game ID (For Table)
                return DB.executeQuery("SELECT r.review_content AS `Review Content`, u.username AS `Username` FROM review r, user u WHERE r.user_id = u.user_id AND r.game_id = " + data.get(i++) );
            case 14: // Check whether a specific game is already on the user wishlist
                return DB.executeQuery("SELECT COUNT(*) AS 'mycount' FROM wishlist WHERE user_id = " + data.get(i++) + " AND game_id = " + data.get(i++) + ";");
            case 15: // Get checked items (2x)
                return DB.executeQuery("SELECT g.game_id AS `Game ID`, g.game_name AS `Game Name`, g.price AS `Price`, d.developer_name AS `Developer Name`, p.publisher_name AS `Publisher Name`, pl.platform_name AS `Platform`, t.tag_name AS `Tag` FROM game g, developer d, publisher p, tag t, platform_list pl WHERE g.developer_id = d.developer_id AND g.publisher_id = p.publisher_id AND g.tag_id = t.tag_id AND g.platform_id = pl.platform_id AND " + data.get(i++) + " LIKE \"%"+ data.get(i++) + "%\" AND " + data.get(i++) + " LIKE \"%" + data.get(i++) + "%\";");
            case 16: // Get checked items and sort
                return DB.executeQuery("SELECT g.game_id AS `Game ID`, g.game_name AS `Game Name`, g.price AS `Price`, d.developer_name AS `Developer Name`, p.publisher_name AS `Publisher Name`, pl.platform_name AS `Platform`, t.tag_name AS `Tag` FROM game g, developer d, publisher p, tag t, platform_list pl WHERE g.developer_id = d.developer_id AND g.publisher_id = p.publisher_id AND g.tag_id = t.tag_id AND g.platform_id = pl.platform_id AND " + data.get(i++) + " LIKE \"%"+ data.get(i++) + "%\" ORDER BY " + data.get(i++) + " " + data.get(i++) + ";");
            case 17: // Get all checked items and sort it
                return DB.executeQuery("SELECT g.game_id AS `Game ID`, g.game_name AS `Game Name`, g.price AS `Price`, d.developer_name AS `Developer Name`, p.publisher_name AS `Publisher Name`, pl.platform_name AS `Platform`, t.tag_name AS `Tag` FROM game g, developer d, publisher p, tag t, platform_list pl WHERE g.developer_id = d.developer_id AND g.publisher_id = p.publisher_id AND g.tag_id = t.tag_id AND g.platform_id = pl.platform_id AND " + data.get(i++) + " LIKE \"%"+ data.get(i++) + "%\" AND " + data.get(i++) + " LIKE \"%" + data.get(i++) + "%\" ORDER BY " + data.get(i++) + " " + data.get(i++) + ";");
            case 18: // Get specific developer name
                return DB.executeQuery("SELECT developer_name FROM developer WHERE developer_id = " + data.get(i++) + ";");
            case 19: // Get specific publisher name
                return DB.executeQuery("SELECT publisher_name FROM publisher WHERE publisher_id = " + data.get(i++) + ";");
            case 20: // Get specific tag name
                return DB.executeQuery("SELECT tag_name FROM tag WHERE tag_id = " + data.get(i++) + ";");
            case 21: // Get specific platform name
                return DB.executeQuery("SELECT platform_name FROM platform_lisy WHERE platform_id = " + data.get(i++) + ";");
                
        }
        i = 0;
        return null;
    }
    public static ResultSet get(int queryNo) // Quick Get, a standalone SQL statement that does not require any data. Usually don't refer to any specific data
    {
        switch(queryNo)
        {
            default:
                System.out.println("ERROR DEVELOPMENT!");
            case 1: // Get all games (Filling HomeScreen Table)
                return DB.executeQuery("SELECT g.game_id AS `Game ID`, g.game_name AS `Game Name`, g.price AS `Price`, d.developer_name AS `Developer Name`, p.publisher_name AS `Publisher Name`,  pl.platform_name AS `Platform`, t.tag_name AS `Tag` FROM game g, developer d, publisher p, tag t, platform_list pl WHERE g.developer_id = d.developer_id AND g.publisher_id = p.publisher_id AND g.tag_id = t.tag_id AND g.platform_id = pl.platform_id;");
            case 2: // Get all Developer Names
                return DB.executeQuery("SELECT * FROM `developer`;");
            case 3: // Get all Publisher Names
                return DB.executeQuery("SELECT * FROM `publisher`;");
            case 4: // Get all Platform Names
                return DB.executeQuery("SELECT * FROM `platform_list`;");
            case 5: // Get Latest Game ID
                return DB.executeQuery("SELECT max(game_id) AS `Game ID` FROM game;");
            case 6: // Get Latest Dev ID
                return DB.executeQuery("SELECT max(developer_id) AS `Developer ID` FROM developer;");
            case 7: // Get Latest Publisher ID
                return DB.executeQuery("SELECT max(publisher_id) AS `Publisher ID` FROM publisher;");
            case 8: // Get Game Tags
                return DB.executeQuery("SELECT tag_id AS `Tag ID`, tag_name AS `Tag Name` FROM tag;");
            case 9: // Get Game ID and Name Only
                return DB.executeQuery("SELECT `game_id`, `game_name` FROM `game` ORDER BY `game_id` ASC;");
            case 10: // Get specific game's stock
                return DB.executeQuery("SELECT `stock` FROM `game` WHERE ");
            case 11: // Get all Tag Names
                return DB.executeQuery("SELECT * FROM `tag`;");
        }
    }
    
    public static int update(int queryNo, ArrayList<String> data) // Updates a record
    {
        int i = 0;
        switch(queryNo)
        {
            case 1: // Add a game to cart
                return DB.executeUpdate("INSERT INTO cart VALUES (" + data.get(i++)+", " + data.get(i++) + ", " + data.get(i++) + ");");
            case 2: // Set Cart quantity
                return DB.executeUpdate("UPDATE cart SET qty = "+data.get(i++)+" WHERE game_id = " + data.get(i++) + "user_id = " + data.get(i++) + ";");
            case 3: // Set old game stock into new one after
                return DB.executeUpdate("UPDATE game SET stock = "+data.get(i++)+" WHERE game_id = "+data.get(i++)+";");
            case 4: // Add a game to wishlist
                return DB.executeUpdate("INSERT INTO wishlist VALUES (" + data.get(i++) + ", " + data.get(i++) + ");");
            case 5: // Updates ALL Game infomation
                return DB.executeUpdate("UPDATE `game` SET `game_name` = '"+data.get(i++)+"', `price` = '"+data.get(i++)+"', `stock` = '"+data.get(i++)+"', `developer_id` = '"+data.get(i++)+"', `publisher_id` = '"+data.get(i++)+"', `description` = '"+data.get(i++)+"', `release_date` = '"+data.get(i++)+"', `platform_id` = '"+data.get(i++)+"', `tag_id` = '"+data.get(i++)+"' WHERE `game`.`game_id` = " + data.get(i++) + ";");
            case 6: // Update specific Developer Name
                return DB.executeUpdate("UPDATE `developer` SET `developer_name` = '" + data.get(i++) + "' WHERE `developer_id` = " + data.get(i++) + ";");
            case 7: // Update specific Publisher Name
                return DB.executeUpdate("UPDATE `publisher` SET `publisher_name` = '" + data.get(i++) + "' WHERE `publisher_id` = " + data.get(i++) + ";");
            case 8: // Update specific Tag Name
                return DB.executeUpdate("UPDATE `tag` SET `tag_name` = '" + data.get(i++) + "' WHERE `tag_id` = " + data.get(i++) + ";");
            case 9: // Update specific Platform Name
                return DB.executeUpdate("UPDATE `platform_list` SET `platform_name` = '" + data.get(i++) + "' WHERE `platform_id` = " + data.get(i++) + ";"); 
        }
        i = 0;
        return 0;
    }
    
    public static int delete(int queryNo, ArrayList<String> data) // Deletes a specific data
    {
        int i = 0;
        switch(queryNo)
        {
            case 1: // Delete WishList from specific user
                return DB.executeUpdate("DELETE FROM `wishlist` WHERE user_id = " + data.get(i++) + ";");
            case 2: // Delete Cart from specific user
                return DB.executeUpdate("DELETE FROM `cart` WHERE user_id = " + data.get(i++) + ";");
            case 3: // Delete specific Game from Cart from specific user
                return DB.executeUpdate("DELETE FROM `cart` WHERE user_id = " + data.get(i++) + " AND game_id = " + data.get(i++) + ";");
            case 4: // Delete specific Game from Wishlist from specific user
                return DB.executeUpdate("DELETE FROM `wishlist` WHERE user_id = " + data.get(i++) + " AND game_id = " + data.get(i++) + ";");
            case 5: // Delete all reviews
                return DB.executeUpdate("DELETE FROM `cart` WHERE user_id = " + data.get(i++) + ";");
            case 6: // Delete specific review
                return DB.executeUpdate("DELETE FROM `review` WHERE user_id = " + data.get(i++) + " AND `game_id` = " + data.get(i++)+";");
            case 7: // Delete a developer
                return DB.executeUpdate("DELETE FROM `developer` WHERE `developer_id` = " + data.get(i++) + ";");
            case 8: // Delete a publisher
                return DB.executeUpdate("DELETE FROM `publisher` WHERE `publisher_id` = " + data.get(i++) + ";");
            case 9: // Delete a tag
                return DB.executeUpdate("DELETE FROM `tag` WHERE `tag_id` = " + data.get(i++) + ";");
            case 10: // Delete a Platform
                return DB.executeUpdate("DELETE FROM `platform_list` WHERE `platform_id` = " + data.get(i++) + ";");
                
        }
        i = 0;
        return 0;
    }
    
    public static int insert(int queryNo, ArrayList<String> data) // Insert specific Data
    {
        int i = 0;
        switch(queryNo)
        {
            case 1: // Inserting Game
                return DB.executeUpdate("INSERT INTO `game` (`game_name`, `price`, `stock`, `developer_id`, `publisher_id`, `description`, `release_date`, `platform_id`, `tag_id`) VALUES ('"+data.get(i++)+"', "+data.get(i++)+", "+data.get(i++)+", '"+data.get(i++)+"', '"+data.get(i++)+"', '"+data.get(i++)+"', '"+data.get(i++)+"', '"+data.get(i++)+"', '"+data.get(i++)+"');");
            case 2: // Insert new Developer
                return DB.executeUpdate("INSERT INTO `developer` (`developer_name`) VALUES ('"+data.get(i++)+"');");
            case 3: // Insert new Publisher
                return DB.executeUpdate("INSERT INTO `publisher` (`publisher_name`) VALUES ('"+data.get(i++)+"');");
            case 4: // Insert new Tag
                return DB.executeUpdate("INSERT INTO `tag` (`tag_name`) VALUES ('"+data.get(i++)+"');");
            case 5: // Insert new Platform
                return DB.executeUpdate("INSERT INTO `platform_list` (`platform_name`) VALUES ('"+data.get(i++)+"');");
            case 6: // Insert new User with Credit Card
                return DB.executeUpdate("INSERT INTO user (username, fullname, password, email, creditcard, permission_group) values ('"+data.get(i++)+"', '"+data.get(i++)+"', '"+data.get(i++)+"', '"+data.get(i++)+"', '"+data.get(i++)+"', "+ data.get(i++) + ")");
            case 7: // Insert new User without Credit Card
                return DB.executeUpdate("INSERT INTO user (username, fullname, password, email, permission_group) values ('"+data.get(i++)+"', '"+data.get(i++)+"', '"+data.get(i++)+"', '"+data.get(i++)+"', " +data.get(i++)+")");
        }
        i = 0;
        return 0;
    }
}
