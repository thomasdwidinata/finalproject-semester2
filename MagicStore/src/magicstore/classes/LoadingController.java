/*

Editor : Thomas Dwi Dinata

*/

package magicstore.classes;

import magicstore.gui.Loading;

public final class LoadingController
{
    private static Loading l;
    
    public static void setFrame(Loading newFrame) // To get new Loading JFrame
    {
        l = newFrame;
    }
    
    public static Loading getLoad() // Returning Loading JFrame
    {
        return l;
    }
    
    public static void LoadingWindow(String s, String ss, boolean useButton, int state) // Setting up new model of JFrame with desired arguments
    {
        l.setText(s);
        l.setSub(ss);
        l.setDefaultCloseOperation(0);
        l.buttonShow(useButton);
        switch(state)
        {
            default: l.buttonShow(false); break;
            case 1: l.setButtonText("Reconfigure Database Connection");
        }
        l.setState(state);
        l.setVisible(true);
    }
    
    public static void disposeWindow() // Deletes loading window
    {
        l.dispose();
    }
    
    public static void closeableWindow() // Allows window closing
    {
        l.setDefaultCloseOperation(2);
    }
    
    public static void disableAlwaysTop() // Disable Always On Top
    {
        l.setAlwaysOnTop(false);
    }
}