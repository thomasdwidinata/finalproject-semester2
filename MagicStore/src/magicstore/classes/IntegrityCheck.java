/*

Editor : Thomas Dwi Dinata

*/

package magicstore.classes;

public final class IntegrityCheck // Input Sanitiser
{
    public static boolean isAlpha(String string)
    {
        return string.matches("[a-zA-Z0-9]+");
    }
    
    public static boolean isAlphaSpace(String string)
    {
        return string.matches("[a-zA-Z0-9 ]+");
    }
    
    public static boolean isNumber(String string)
    {
        return string.matches("[0-9]+");
    }

    public static boolean isAlphabet(String string)
    {
        return string.matches("[a-zA-Z]+");
    }
    
    public static boolean isYear(String string)
    {
        return string.matches("[1990-2110]+");
    }
    
    public static boolean isMonth(String string)
    {
        return string.matches("[1-12]+");
    }
    
    public static boolean isDay(String string)
    {
        return string.matches("[1-31]+");
    }
    
    public static boolean isFloat(String string)
    {
        return string.matches("[0-9.]+");
    }
    
    public static boolean isEmptyString(String string)
    {
        return string.equals("");
    }
}
