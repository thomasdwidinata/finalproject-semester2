/*

Editor : Thomas Dwi Dinata

*/

package magicstore.classes;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/*

    This class is used for Filling Tables and ComboBoxes, easier because these methods will return the model of the Table or ComboBox

*/

public class dataFill
{
    public static DefaultComboBoxModel fillBoxNoFormatting(String s, int i, int mode) // Mode = 0 for ID only, Mode 1 for Name only
    {
        DefaultComboBoxModel newModel = new DefaultComboBoxModel();
        try
        {
            ResultSet rs = QueryProcessor.get(i);
            while(rs.next())
            {
                switch(mode)
                {
                    case 0:
                        String id = rs.getString(s + "_id");
                        newModel.addElement(id);
                        break;
                    case 1:
                        String tableString = rs.getString(s + "_name");
                        newModel.addElement(tableString);
                        break;
                }
            }
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Error getting data from Database!", "Database retrieval", JOptionPane.ERROR_MESSAGE);
        }
        return newModel;
    }
    
    public static DefaultComboBoxModel fillBox(String s, int i)
    {
        DefaultComboBoxModel newModel = new DefaultComboBoxModel();
        try
        {
            ResultSet rs = QueryProcessor.get(i);
            while(rs.next())
            {
                String tableString = rs.getString(s + "_name");
                String id = rs.getString(s + "_id");
                newModel.addElement(id + "-" + tableString);
            }
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Error getting data from Database!", "Database retrieval", JOptionPane.ERROR_MESSAGE);
        }
        return newModel;
    }
    
    public static DefaultTableModel fillTable(int query) // To fill tables with automatic get queries
    {
        try
        {
            DefaultTableModel dtm = new DefaultTableModel();
            ResultSet rs = QueryProcessor.get(query);
            ResultSetMetaData rmd = rs.getMetaData();
            int colTotal = rmd.getColumnCount();
            for(int i=0; i<colTotal; i++) 
                dtm.addColumn(rmd.getColumnLabel(i+1));
            while(rs.next())
            {
                Object[] objects = new Object[colTotal];
                for(int i=1; i<=colTotal; i++)
                {
                    objects[i-1] = rs.getObject(i);
                }
                dtm.addRow(objects);
            }
            return dtm;
        }
        catch (SQLException ex)
        {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, "Data refreshing error! Error: " + ex, "Filling Table", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }
    
    public static DefaultTableModel fillTable(int query, ArrayList<String> alist) // To fill tables with specific information which will be sent to the QueryProcessor
    {
        try
        {
            DefaultTableModel dtm = new DefaultTableModel();
            ResultSet rs = QueryProcessor.getCustom(query, alist);
            ResultSetMetaData rmd = rs.getMetaData();
            int colTotal = rmd.getColumnCount();
            for(int i=0; i<colTotal; i++) 
                dtm.addColumn(rmd.getColumnLabel(i+1));
            while(rs.next())
            {
                Object[] objects = new Object[colTotal];
                for(int i=1; i<=colTotal; i++)
                {
                    objects[i-1] = rs.getObject(i);
                }
                dtm.addRow(objects);
            }
            return dtm;
        }
        catch (SQLException ex)
        {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, "Data refreshing error! Error: " + ex, "Filling Table", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }
    
    
    public static DefaultTableModel fillTable(ResultSet rs) // Filling table with desired ResultSet
    {
        try
        {
            DefaultTableModel dtm = new DefaultTableModel();
            ResultSetMetaData rmd = rs.getMetaData();
            int colTotal = rmd.getColumnCount();
            for(int i=0; i<colTotal; i++) 
                dtm.addColumn(rmd.getColumnLabel(i+1));
            while(rs.next())
            {
                Object[] objects = new Object[colTotal];
                for(int i=1; i<=colTotal; i++)
                {
                    objects[i-1] = rs.getObject(i);
                }
                dtm.addRow(objects);
            }
            return dtm;
        }
        catch (SQLException ex)
        {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, "Data refreshing error! Error: " + ex, "Filling Table", JOptionPane.ERROR_MESSAGE);
        }
        catch (Exception e)
        {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "Data refreshing error! Received an invalid table model or data! Error: " + e, "Filling Table", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }
    
    public static DefaultComboBoxModel fillBox(int query) // To fill ComboBox with automatic get queries
    {
        try
        {
            DefaultComboBoxModel newModel = new DefaultComboBoxModel();
            ResultSet rs = QueryProcessor.get(query);
            ResultSetMetaData rmd = rs.getMetaData();
            int colTotal = rmd.getColumnCount();
            for(int i=0; i<colTotal; i++) 
                newModel.addElement(rmd.getColumnLabel(i+1));
            return newModel;
        }
        catch(SQLException ex)
        {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, "Data refreshing error! Error: " + ex, "Filling Box", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }
    
    public static DefaultComboBoxModel fillBox(ArrayList<String> data) // To fill ComboBox with several values
    {
        DefaultComboBoxModel newModel = new DefaultComboBoxModel();
        int colTotal = data.size();
        for(int i=0; i<colTotal; i++) 
            newModel.addElement(data.get(i));
        return newModel;
    }
}
