/*

Magic Store
By : Luffandri (1901498321), Ramada Atyusa (1901499280), Steven Muliamin (1901498763), Thomas Dwi Dinata (1901498151)

*/


package magicstore.classes;

import magicstore.gui.*;

public class MagicStore
{
    public static String appVersion = "0.1 BETA";
    public static void main(String[] args)
    {
        new ProgramConfig();
        Loading l = new Loading();
        LoadingController lc = new LoadingController();
        lc.setFrame(l);
        lc.LoadingWindow("Starting up 'Magic Store' Application...", "Connecting to Database...", false, 0);
        lc.disableAlwaysTop();
        new DB();
        if(QueryProcessor.defaultConnect())
        {
            lc.disposeWindow();
            new HomeScreen();
        }
        else
        {
            lc.LoadingWindow("Starting up failed!", "Database connection failed! Please restart application!", true, 1);
            lc.closeableWindow();
        }
    }
    public static String getVersion() // Returns the version, used for About JFrame
    {
        return appVersion;
    }
}
