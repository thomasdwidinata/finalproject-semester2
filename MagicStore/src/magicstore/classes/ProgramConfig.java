/*

Editor : Thomas Dwi Dinata

*/

package magicstore.classes;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public final class ProgramConfig
{
    private static Config c; // To point to the created Config Object
    private static String filename; // To save the path file of the configuration file
    private static ArrayList<String> configs; // Saves any configurations into ArrayList for easy expanding
    
    public static String getFileName() // Returns path file of the configuration file
    {
        return filename;
    }
    
    public static void checkOS() // Checking OS for Configuration file Path
    {
        int os = 0;
        filename = new String();
        String ostype = System.getProperty("os.name");
        switch (ostype.substring(0,1))
        {
            default:
                System.out.println("Unsupported OS! Sorry, program will be terminated!");
                JOptionPane.showMessageDialog(null, "Unsupported OS! Sorry, program will be terminated!", "System Check", JOptionPane.ERROR_MESSAGE);
                System.exit(-2);
                break;
            case "W": os = 1; break;
            case "M": os = 2; break;
            case "L": os = 2; break;
        }
        String userHome = System.getProperty("user.home");
        switch(os)
        {
            case 1: filename = userHome.concat("\\magicstore.cfg"); break;
            case 2: filename = userHome.concat("/magicstore.cfg"); break; 
        }
    }
    
    public static void setConfig(ArrayList<String> alist) // Set a configuration to the Config Class
    {
        c.setConfig(alist);
    }
    
    public static Config getObject() // Getting the Config Object
    {
        return c;
    }
    
    public ProgramConfig()
    {
        // Reading Configuration file
        boolean configAvailable = false;
        checkOS();
        try
        {
            FileReader fileRead = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(fileRead);
            configs = new ArrayList<String>();
            for(int i = 0; i < 4; ++i)
            {
                configs.add(bufferedReader.readLine());
            }
            bufferedReader.close();
            c = new Config();
            c.setConfig(configs);
            configAvailable = true;
        }
        catch (FileNotFoundException ex)
        {
            JOptionPane.showMessageDialog(null, "No configuration found! Default configuraion will be used!", "Program Configuration", JOptionPane.INFORMATION_MESSAGE);
            configAvailable = false;
        }
        catch(IOException ex)
        {
            JOptionPane.showMessageDialog(null, "Configuration file read error. Make sure you have the right permission to access the configuration file! Default configuraion will be used!", "Program Configuration", JOptionPane.INFORMATION_MESSAGE);
            configAvailable = false;
        }
        catch (Exception ex)
        {
            System.out.println(ex);
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "An error has occured! Error : " + ex, "Program Configuration", JOptionPane.ERROR_MESSAGE);
            configAvailable = false;
        }
        
        // Check whether the configuration is available or not, if no it will use the default configuration by this program
        if(!(configAvailable))
        {
            c = new Config();
            c.defaultConfig();
        }
        
    }
    
    public static ArrayList<String> getSearchableItem() // Get Searchable items, used for HomeScreen to match between database tables and program for searching method
    {
        return c.getSearchItem();
    }
    
    public static ArrayList<String> getServerConfig() // To get all server configurations from the Object
    {
        return c.getAllConfig();
    }
}
