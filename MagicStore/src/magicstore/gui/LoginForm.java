package magicstore.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import magicstore.classes.GUIController;
import magicstore.classes.LoginCredentials;

/*
    EDITOR: THOMAS
*/

//THIS CLASS IS FOR THE 'LOGIN' WINDOW


public class LoginForm extends JFrame
{
    private JButton loginButton = new JButton("Login");
    private JButton cancelButton = new JButton("Cancel");
    private JPanel southPanel = new JPanel(new FlowLayout());
    private JPanel centrePanel = new JPanel(new GridLayout(3,2));
    private JPanel northPanel = new JPanel(new GridLayout(3,2));
    private JLabel labelUsername = new JLabel("Username : ");
    private JLabel labelPassword = new JLabel("Password : ");
    private JTextField txtUsername = new JTextField();
    private JPasswordField txtPassword = new JPasswordField();
            
    private boolean isLoginValid()
    {
        return LoginCredentials.isLoginValid();
    }
    
    private void closer()
    {
        GUIController.setEnabled(true);
        this.dispose();
    }
    
    public LoginForm()
    {
        this.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
               closer();
            }
        });
        if(!isLoginValid())
        {
            this.setTitle("User Login");
            this.setLocationRelativeTo(null);
            this.setSize(250,235);
            this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            this.setLayout(new BorderLayout());
            this.setResizable(false);
            
            centrePanel.add(labelUsername);
            centrePanel.add(txtUsername);

            centrePanel.add(labelPassword);
            centrePanel.add(txtPassword);
            
            this.add(centrePanel, BorderLayout.CENTER);
            
            southPanel.add(loginButton);
            southPanel.add(cancelButton);
            
            this.add(southPanel, BorderLayout.SOUTH);

            loginButton.addActionListener(new java.awt.event.ActionListener()
            {
                public void actionPerformed(java.awt.event.ActionEvent evt)
                {
                    loginButtonActionPerformed(evt);
                }
            });
            
            cancelButton.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt)
                {
                    cancelButtonActionPerformed(evt);
                }
            });
            
            this.setVisible(true);
        }
        else
        {
            JOptionPane.showMessageDialog(rootPane, "You are logged in. Please logout to login to another account. Simultaneous login is not allowed!", "Login", JOptionPane.INFORMATION_MESSAGE);
            this.dispose();
            GUIController.setEnabled(true);
        }
    }
    
    private void loginButtonActionPerformed(java.awt.event.ActionEvent evt)
    {
        ArrayList<String> arraydata = new ArrayList<String>();
        int userid[];
        arraydata.add(txtUsername.getText()); // SPECIAL ARRAYLIST! Index 0 for username, Index 1 for Password
        arraydata.add(String.valueOf(txtPassword.getPassword()));
        userid = LoginCredentials.checkLogin(arraydata);
        System.out.println(userid[1]);
        if(userid[0] > 0)
        {
            this.dispose();
            if(userid[1] == 2)
            {
                GUIController.dispose();
                new AdminConsole();
            }
            else
            {
                GUIController.setEnabled(true);
                HomeScreen.checkLoginStatus();
            }
        }
        else
            JOptionPane.showMessageDialog(null, "Username/Password invalid!");
    }
    
    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt)
    {
        LoginCredentials.dropLogin();
        this.dispose();
        GUIController.setEnabled(true);
        HomeScreen.checkLoginStatus();
    }
}
