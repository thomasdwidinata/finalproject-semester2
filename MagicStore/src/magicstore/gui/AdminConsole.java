package magicstore.gui;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import magicstore.classes.IntegrityCheck;
import magicstore.classes.LoginCredentials;
import magicstore.classes.QueryProcessor;
import magicstore.classes.dataFill;

/*
    EDITOR: THOMAS
*/

//THIS CLASS IS FOR ADMIN FUNCTION TO EDIT OR INSERT DATA

public final class AdminConsole extends javax.swing.JFrame
{
    private int modeState;
    
    public AdminConsole()
    {
        modeState = 0;
        
        initComponents();
        
        buttonGroup1.setSelected(addMode.getModel(),true);
        this.setTitle("Administrator Console");
        
        username.setText(LoginCredentials.getLogin());
        fillAllBox();
        buttonStateCheck();
        
        this.setLocationRelativeTo(null);  
        this.setVisible(true);
    }
    
    private int getTabIndex()
    {
        return TAB.getSelectedIndex();
    }
    
    private void buttonStateCheck()
    {
        switch(getTabIndex())
        {
            case 0:
                actionButton.setText("Add Data");
            case 1:
                actionButton.setText("Add Developer");
            case 2:
                actionButton.setText("Add Publisher");
            case 3:
                actionButton.setText("Add Tag");
        }
    }
    
    private void fillAllBox()
    {
        //SETS ALL THE INFO FOR THE ADMIN
        
        DefaultComboBoxModel devModel = dataFill.fillBox("developer",2);
        devBox.setModel(devModel);
        devIDEdit.setModel(devModel);
        devModel = dataFill.fillBox("publisher", 3);
        
        pubBox.setModel(devModel);
        pubIDEdit.setModel(devModel);
        platformBox.setModel(dataFill.fillBox("platform", 4));
        gameEditID.setModel(dataFill.fillBox("game", 9));
        tagBox.setModel(dataFill.fillBox("tag", 11));
        devIDEdit.setModel(dataFill.fillBoxNoFormatting("developer", 2, 0));
        pubIDEdit.setModel(dataFill.fillBoxNoFormatting("publisher", 3, 0));
        tagIDEdit.setModel(dataFill.fillBoxNoFormatting("tag", 11, 0));
        platIDEdit.setModel(dataFill.fillBoxNoFormatting("platform", 4, 0));
        devTable.setModel(dataFill.fillTable(2));
        pubTable.setModel(dataFill.fillTable(3));
        tagTable.setModel(dataFill.fillTable(11));
        platTable.setModel(dataFill.fillTable(4));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jSeparator1 = new javax.swing.JSeparator();
        TAB = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        gameField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        devBox = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        priceField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        stockField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        pubBox = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        platformBox = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        gameDesc = new javax.swing.JTextArea();
        showGames = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        gameEditID = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        yearField = new javax.swing.JTextField();
        dayField = new javax.swing.JTextField();
        monthField = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tagBox = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        devIDEdit = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        devTable = new javax.swing.JTable();
        devField = new javax.swing.JTextField();
        devDelete = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        pubIDEdit = new javax.swing.JComboBox<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        pubTable = new javax.swing.JTable();
        pubField = new javax.swing.JTextField();
        pubDelete = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        tagIDEdit = new javax.swing.JComboBox<>();
        jScrollPane4 = new javax.swing.JScrollPane();
        tagTable = new javax.swing.JTable();
        tagField = new javax.swing.JTextField();
        tagDelete = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        platIDEdit = new javax.swing.JComboBox<>();
        jScrollPane5 = new javax.swing.JScrollPane();
        platTable = new javax.swing.JTable();
        platField = new javax.swing.JTextField();
        platDelete = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        actionButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        welcomeText = new javax.swing.JLabel();
        username = new javax.swing.JLabel();
        editMode = new javax.swing.JRadioButton();
        addMode = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        TAB.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Game Name : ");

        jLabel3.setText("Developer : ");

        devBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        devBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                devBoxActionPerformed(evt);
            }
        });

        jLabel4.setText("Price :");

        jLabel5.setText("Stock :");

        jLabel7.setText("Publisher :");

        pubBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel11.setText("Platform :");

        platformBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel9.setText("Release Date :");

        jLabel10.setText("Game Description :");

        gameDesc.setColumns(20);
        gameDesc.setRows(5);
        jScrollPane1.setViewportView(gameDesc);

        showGames.setText("Check Data");
        showGames.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showGamesActionPerformed(evt);
            }
        });

        jLabel14.setText("Edit ID No. :");

        gameEditID.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        gameEditID.setEnabled(false);
        gameEditID.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                gameEditIDItemStateChanged(evt);
            }
        });
        gameEditID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gameEditIDActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel8.setText("/");

        jLabel16.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel16.setText("/");

        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("Year");

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("Month");

        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("Day");

        jLabel2.setText("Tag :");

        tagBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(priceField, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(stockField, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(gameField, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel7)
                            .addComponent(jLabel11))
                        .addGap(21, 21, 21)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tagBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pubBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(devBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(platformBox, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dayField, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(monthField, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(yearField, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel2))
                .addGap(63, 63, 63)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(gameEditID, 0, 783, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(showGames))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(gameEditID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showGames)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(gameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(priceField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(stockField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(devBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(pubBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(platformBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(tagBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(jLabel8)
                            .addComponent(jLabel16)
                            .addComponent(yearField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dayField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(monthField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20)
                            .addComponent(jLabel21)
                            .addComponent(jLabel22))))
                .addContainerGap(48, Short.MAX_VALUE))
        );

        TAB.addTab("Game", jPanel2);

        jLabel13.setText("Developer Name :");

        jLabel15.setText("Edit ID No. :");

        devIDEdit.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        devIDEdit.setEnabled(false);
        devIDEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                devIDEditActionPerformed(evt);
            }
        });

        devTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(devTable);

        devDelete.setText("Delete");
        devDelete.setEnabled(false);
        devDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                devDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addGap(18, 18, 18)
                                .addComponent(devField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addGap(55, 55, 55)
                                .addComponent(devIDEdit, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(devDelete)
                        .addContainerGap())
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1330, Short.MAX_VALUE)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(devField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(devDelete))
                .addGap(14, 14, 14)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(devIDEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                .addContainerGap())
        );

        TAB.addTab("Developer", jPanel3);

        jLabel18.setText("Publisher Name :");

        jLabel19.setText("Edit ID No. :");

        pubIDEdit.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        pubIDEdit.setEnabled(false);
        pubIDEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pubIDEditActionPerformed(evt);
            }
        });

        pubTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(pubTable);

        pubDelete.setText("Delete");
        pubDelete.setEnabled(false);
        pubDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pubDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(jLabel18)
                                .addGap(18, 18, 18)
                                .addComponent(pubField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(jLabel19)
                                .addGap(52, 52, 52)
                                .addComponent(pubIDEdit, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pubDelete)
                        .addContainerGap())
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1330, Short.MAX_VALUE)))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(pubField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pubDelete))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(pubIDEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                .addContainerGap())
        );

        TAB.addTab("Publisher", jPanel7);

        jLabel26.setText("Tag Name :");

        jLabel27.setText("Edit ID No. :");

        tagIDEdit.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        tagIDEdit.setEnabled(false);
        tagIDEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tagIDEditActionPerformed(evt);
            }
        });

        tagTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tagTable);

        tagDelete.setText("Delete");
        tagDelete.setEnabled(false);
        tagDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tagDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel26)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel27)
                                .addGap(44, 44, 44)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(tagField, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                                    .addComponent(tagIDEdit, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(tagDelete)
                        .addContainerGap())
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1330, Short.MAX_VALUE)))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(tagField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tagDelete))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(tagIDEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                .addContainerGap())
        );

        TAB.addTab("Tag", jPanel8);

        jLabel28.setText("Platform Name :");

        jLabel29.setText("Edit ID No. :");

        platIDEdit.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        platIDEdit.setEnabled(false);
        platIDEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                platIDEditActionPerformed(evt);
            }
        });

        platTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(platTable);

        platDelete.setText("Delete");
        platDelete.setEnabled(false);
        platDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                platDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel28)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addComponent(jLabel29)
                                .addGap(44, 44, 44)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(platField, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                                    .addComponent(platIDEdit, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(platDelete)
                        .addContainerGap())
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1330, Short.MAX_VALUE)))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(platField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(platDelete))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(platIDEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                .addContainerGap())
        );

        TAB.addTab("Platform", jPanel9);

        jLabel6.setText("You may able to make another Administrator Account, all administrators will have the same console like this.");

        jLabel12.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel12.setText("To get started, click at this button -->");

        jButton2.setText("Register new Admin");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel17.setText("You may also able to edit Database Connection and save the current configuration,");

        jLabel23.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel23.setText("To get started, click at this button -->");

        jButton3.setText("Edit Database Connection");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(34, 34, 34)
                        .addComponent(jButton2))
                    .addComponent(jLabel17)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addGap(18, 18, 18)
                        .addComponent(jButton3)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jButton2))
                .addGap(47, 47, 47)
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(jButton3))
                .addContainerGap(229, Short.MAX_VALUE))
        );

        TAB.addTab("Program Configuration", jPanel1);

        actionButton.setText("ACTION+TAB");
        actionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionButtonActionPerformed(evt);
            }
        });

        jButton1.setText("Logout");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        welcomeText.setText("Welcome, ");

        username.setText(" ");

        buttonGroup1.add(editMode);
        editMode.setText("Edit Mode");
        editMode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editModeActionPerformed(evt);
            }
        });

        buttonGroup1.add(addMode);
        addMode.setText("Add Mode");
        addMode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addModeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(actionButton)
                .addGap(18, 18, 18)
                .addComponent(welcomeText)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(username, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(addMode)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(editMode)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(actionButton)
                    .addComponent(jButton1)
                    .addComponent(welcomeText)
                    .addComponent(username)
                    .addComponent(editMode)
                    .addComponent(addMode))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(TAB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TAB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private ArrayList<String> packGameData()
    {
        ArrayList<String> data = new ArrayList<String>();
        data.add(gameField.getText());
        data.add(priceField.getText());
        data.add(stockField.getText());
        data.add(String.valueOf(devBox.getSelectedItem()).substring(0, 3));
        data.add(String.valueOf(pubBox.getSelectedItem()).substring(0, 3));
        data.add(gameDesc.getText());
        data.add(yearField.getText()+"-"+monthField.getText()+"-"+dayField.getText());
        data.add(String.valueOf(platformBox.getSelectedItem()).substring(0, 3));
        data.add(String.valueOf(tagBox.getSelectedItem()).substring(0, 3));
        data.add(String.valueOf(gameEditID.getSelectedItem()).substring(0,3));
        return data;
    }
    
    private void boxSearch(JComboBox e, String comparison)
    {
        for(int i = 0; i < e.getItemCount(); ++i)
        {
            e.setSelectedIndex(i);
            if(String.valueOf(e.getSelectedItem()).equals(comparison))
                return;
        }
    }
    
    private void refill()
    {
        ArrayList<String> alist = new ArrayList<String>();
        alist.add(String.valueOf(gameEditID.getSelectedItem()).substring(0, 3));
        try
        {
            ResultSet rs = QueryProcessor.getCustom(9, alist);
            rs.last();
            gameField.setText(rs.getString("game_name"));
            priceField.setText(rs.getString("price"));
            stockField.setText(rs.getString("stock"));
            
            String dev = rs.getString("developer_id") + "-" + rs.getString("developer_name");
            String pub = rs.getString("publisher_id") + "-" + rs.getString("publisher_name");
            String tag = rs.getString("tag_id") + "-" + rs.getString("tag_name");
            String platform = rs.getString("platform_id") + "-" + rs.getString("platform_name");
            
            boxSearch(devBox, dev);
            boxSearch(pubBox, pub);
            boxSearch(tagBox, tag);
            boxSearch(platformBox, platform);
            
            String date = rs.getString("release_date");
            
            yearField.setText(date.substring(0, 4));
            monthField.setText(date.substring(5, 7));
            dayField.setText(date.substring(8, 10));
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "An error occured! Error: " + ex, "Get Game Info", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void actionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionButtonActionPerformed
        ArrayList<String> data = new ArrayList<String>();
        data.clear();
        int result = 0;
        switch(modeState)
        {
            default:
                JOptionPane.showMessageDialog(rootPane, "An error has occured! Program will now terminated!", "Unknown Mode", JOptionPane.ERROR_MESSAGE);
                System.exit(-1);
            case 0:
                switch(getTabIndex())
                {
                    case 0:
                        if(!checkData())
                        {
                            JOptionPane.showMessageDialog(null, "Please enter valid number and please avoid any special characters!", "Data Add", JOptionPane.ERROR_MESSAGE);
                            break;
                        }   
                        else    
                        result = QueryProcessor.insert(1, packGameData());
                        break;
                    case 1:
                        data.add(devField.getText());
                        result = QueryProcessor.insert(2, data);
                        break;
                        
                    case 2:
                        data.add(pubField.getText());
                        result = QueryProcessor.insert(3, data);
                        break;
                        
                    case 3:
                        data.add(tagField.getText());
                        result = QueryProcessor.insert(4, data);
                        break;
                        
                    case 4:
                        data.add(platField.getText());
                        result = QueryProcessor.insert(5, data);
                        break;
                }
                if(result < 1)
                {
                    JOptionPane.showMessageDialog(rootPane, "An error occured! Data cannot be recorded", "Data Recording", JOptionPane.ERROR_MESSAGE);
                }
                else
                {
                    JOptionPane.showMessageDialog(rootPane, "Data input success!", "Data Recording", JOptionPane.INFORMATION_MESSAGE);
                    clearAllData();
                }
                break;
                
            case 1:
                switch(getTabIndex())
                {
                    case 0:
                        result = QueryProcessor.update(5, packGameData());
                        break;
                        
                    case 1:
                        data.add(devField.getText());
                        data.add(String.valueOf(devIDEdit.getSelectedItem()));
                        result = QueryProcessor.update(6, data);
                        break;
                        
                    case 2:
                        data.add(pubField.getText());
                        data.add(String.valueOf(pubIDEdit.getSelectedItem()));
                        result = QueryProcessor.update(7, data);
                        break;
                        
                    case 3:
                        data.add(tagField.getText());
                        data.add(String.valueOf(tagIDEdit.getSelectedItem()));
                        result = QueryProcessor.update(8, data);
                        break;
                        
                    case 4:
                        data.add(platField.getText());
                        data.add(String.valueOf(platIDEdit.getSelectedItem()));
                        result = QueryProcessor.update(9, data);
                        break;
                        
                }
                if(result < 1)
                {
                    JOptionPane.showMessageDialog(rootPane, "An error occured! Data cannot be recorded", "Data Recording", JOptionPane.ERROR_MESSAGE);
                }
                else
                {
                    JOptionPane.showMessageDialog(rootPane, "Data input success!", "Data Recording", JOptionPane.INFORMATION_MESSAGE);
                    clearAllData();
                }
                break;
        }
        fillAllBox();
    }//GEN-LAST:event_actionButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        LoginCredentials.dropLogin();
        JOptionPane.showMessageDialog(rootPane, "You have been successfuly Logged out", "Admin Console", JOptionPane.INFORMATION_MESSAGE);
        this.dispose();
        new HomeScreen();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void buttonReverse(boolean bol)
    {
        pubIDEdit.setEnabled(bol);
        devIDEdit.setEnabled(bol);
        tagIDEdit.setEnabled(bol);
        platIDEdit.setEnabled(bol);
        platDelete.setEnabled(bol);
        tagDelete.setEnabled(bol);
        pubDelete.setEnabled(bol);
        devDelete.setEnabled(bol);
    }
    
    private void editModeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editModeActionPerformed
        modeCheck();
        modeState = 1;
        actionButton.setText("Edit Game");
        buttonReverse(true);
        refill();
    }//GEN-LAST:event_editModeActionPerformed

    private void addModeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addModeActionPerformed
        modeCheck();
        modeState = 0;
        buttonReverse(false);
        clearAllData();
        actionButton.setText("Add Game");
    }//GEN-LAST:event_addModeActionPerformed

    private void showGamesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showGamesActionPerformed
        new AdminTable();
    }//GEN-LAST:event_showGamesActionPerformed

    private void devBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_devBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_devBoxActionPerformed

    private void gameEditIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gameEditIDActionPerformed
        refill();
    }//GEN-LAST:event_gameEditIDActionPerformed

    private void gameEditIDItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_gameEditIDItemStateChanged
        //JOptionPane.showMessageDialog(null, "edited");
    }//GEN-LAST:event_gameEditIDItemStateChanged

    private void devIDEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_devIDEditActionPerformed
        ArrayList<String> alist = new ArrayList<String>();
        alist.add(String.valueOf(devIDEdit.getSelectedItem()));
        
        try{
            ResultSet rs = QueryProcessor.getCustom(18, alist);
            rs.last();
            
            devField.setText(rs.getString("developer_name"));
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Unable to retrieve data!", "Developer Name Download", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_devIDEditActionPerformed

    private void pubIDEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pubIDEditActionPerformed
        ArrayList<String> alist = new ArrayList<String>();
        alist.add(String.valueOf(pubIDEdit.getSelectedItem()));
        
        try{
            ResultSet rs = QueryProcessor.getCustom(19, alist);
            rs.last();
            
            pubField.setText(rs.getString("publisher_name"));
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Unable to retrieve data! " + ex, "Developer Name Download", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_pubIDEditActionPerformed

    private void tagIDEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tagIDEditActionPerformed
        ArrayList<String> alist = new ArrayList<String>();
        alist.add(String.valueOf(tagIDEdit.getSelectedItem()));
        
        try{
            ResultSet rs = QueryProcessor.getCustom(20, alist);
            rs.last();
            
            tagField.setText(rs.getString("tag_name"));
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Unable to retrieve data!", "Developer Name Download", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_tagIDEditActionPerformed

    private void platIDEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_platIDEditActionPerformed
        ArrayList<String> alist = new ArrayList<String>();
        alist.add(String.valueOf(tagIDEdit.getSelectedItem()));
        
        try{
            ResultSet rs = QueryProcessor.getCustom(21, alist);
            rs.last();
            
            tagField.setText(rs.getString("platform_name"));
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Unable to retrieve data!", "Developer Name Download", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_platIDEditActionPerformed

    private void devDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_devDeleteActionPerformed
        if(JOptionPane.showConfirmDialog(null, "Are you sure to delete selected Developer?", "Remove Developer", JOptionPane.YES_NO_OPTION) == 0)
        {
            ArrayList<String> alist = new ArrayList<String>();
            alist.add(String.valueOf(devIDEdit.getSelectedItem()));
            
            QueryProcessor.delete(7, alist);
            JOptionPane.showMessageDialog(null, "Developer Deleted!", "Remove Developer", JOptionPane.INFORMATION_MESSAGE);
        }
        fillAllBox();
    }//GEN-LAST:event_devDeleteActionPerformed

    private void pubDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pubDeleteActionPerformed
        if(JOptionPane.showConfirmDialog(null, "Are you sure to delete selected Publisher?", "Remove Publisher", JOptionPane.YES_NO_OPTION) == 0)
        {
            ArrayList<String> alist = new ArrayList<String>();
            alist.add(String.valueOf(pubIDEdit.getSelectedItem()));
            
            QueryProcessor.delete(8, alist);
            JOptionPane.showMessageDialog(null, "Publisher Deleted!", "Remove Publisher", JOptionPane.INFORMATION_MESSAGE);
        }
        fillAllBox();
    }//GEN-LAST:event_pubDeleteActionPerformed

    private void tagDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tagDeleteActionPerformed
        if(JOptionPane.showConfirmDialog(null, "Are you sure to delete selected Tag?", "Remove a Tag", JOptionPane.YES_NO_OPTION) == 0)
        {
            ArrayList<String> alist = new ArrayList<String>();
            alist.add(String.valueOf(tagIDEdit.getSelectedItem()));
            
            QueryProcessor.delete(9, alist);
            JOptionPane.showMessageDialog(null, "Tag Deleted!", "Remove a Tag", JOptionPane.INFORMATION_MESSAGE);
        }
        fillAllBox();
    }//GEN-LAST:event_tagDeleteActionPerformed

    private void platDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_platDeleteActionPerformed
        if(JOptionPane.showConfirmDialog(null, "Are you sure to delete selected Platform?", "Remove Platform", JOptionPane.YES_NO_OPTION) == 0)
        {
            ArrayList<String> alist = new ArrayList<String>();
            alist.add(String.valueOf(platIDEdit.getSelectedItem()));
            QueryProcessor.delete(10, alist);
            JOptionPane.showMessageDialog(null, "Platform Deleted!", "Remove Platform", JOptionPane.INFORMATION_MESSAGE);
        }
        fillAllBox();
    }//GEN-LAST:event_platDeleteActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        new Registration(2);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        new ConfigEditor();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void modeCheck()
    {
        if(buttonGroup1.getSelection() == addMode.getModel())
        {
            clearAllData();
            gameEditID.setEnabled(false);
            showGames.setEnabled(false);
        }
        else
        {
            clearAllData();
            gameEditID.setEnabled(true);
            showGames.setEnabled(true);
        }
    }
    
    private boolean checkData()
    {
        if(!(IntegrityCheck.isEmptyString(gameField.getText()) || IntegrityCheck.isNumber(priceField.getText()) || IntegrityCheck.isNumber(stockField.getText()) || IntegrityCheck.isNumber(gameDesc.getText()) || IntegrityCheck.isNumber(dayField.getText()) || IntegrityCheck.isNumber(monthField.getText()) || IntegrityCheck.isNumber(yearField.getText())))
        {
            JOptionPane.showMessageDialog(rootPane, "All fields are mandatory, except Game Cover", "Data Recording", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if(!(IntegrityCheck.isNumber(dayField.getText()) || IntegrityCheck.isNumber((monthField.getText())) || IntegrityCheck.isNumber(yearField.getText()) || IntegrityCheck.isNumber(priceField.getText()) || IntegrityCheck.isNumber(stockField.getText())))
        {
            JOptionPane.showMessageDialog(rootPane, "Invalid Number for Numeric Fields such as Price, Stock, and Date!", "Data Recording", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true; 
    }
    
    private void clearAllData()
    {
        gameField.setText("");
        priceField.setText("");
        stockField.setText("");
        gameDesc.setText("");
        
        devBox.setSelectedIndex(0);
        pubBox.setSelectedIndex(0);
        platformBox.setSelectedIndex(0);
        tagBox.setSelectedIndex(0);
        gameEditID.setSelectedIndex(0);
        
        dayField.setText("");
        monthField.setText("");
        yearField.setText("");
        
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane TAB;
    private javax.swing.JButton actionButton;
    private javax.swing.JRadioButton addMode;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JTextField dayField;
    private javax.swing.JComboBox<String> devBox;
    private javax.swing.JButton devDelete;
    private javax.swing.JTextField devField;
    private javax.swing.JComboBox<String> devIDEdit;
    private javax.swing.JTable devTable;
    private javax.swing.JRadioButton editMode;
    private javax.swing.JTextArea gameDesc;
    private javax.swing.JComboBox<String> gameEditID;
    private javax.swing.JTextField gameField;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField monthField;
    private javax.swing.JButton platDelete;
    private javax.swing.JTextField platField;
    private javax.swing.JComboBox<String> platIDEdit;
    private javax.swing.JTable platTable;
    private javax.swing.JComboBox<String> platformBox;
    private javax.swing.JTextField priceField;
    private javax.swing.JComboBox<String> pubBox;
    private javax.swing.JButton pubDelete;
    private javax.swing.JTextField pubField;
    private javax.swing.JComboBox<String> pubIDEdit;
    private javax.swing.JTable pubTable;
    private javax.swing.JButton showGames;
    private javax.swing.JTextField stockField;
    private javax.swing.JComboBox<String> tagBox;
    private javax.swing.JButton tagDelete;
    private javax.swing.JTextField tagField;
    private javax.swing.JComboBox<String> tagIDEdit;
    private javax.swing.JTable tagTable;
    private javax.swing.JLabel username;
    private javax.swing.JLabel welcomeText;
    private javax.swing.JTextField yearField;
    // End of variables declaration//GEN-END:variables
}