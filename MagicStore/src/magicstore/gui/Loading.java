package magicstore.gui;

/*
    EDITOR: THOMAS
*/

//THIS CLASS IS ONLY FOR LOADING SCREEN

public class Loading extends javax.swing.JFrame
{
    private int loadingState;
    public Loading()
    {
        initComponents();
        this.setTitle("Magic Store Application");
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setVisible(true);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        textCentre = new javax.swing.JLabel();
        textSub = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setAlwaysOnTop(true);

        textCentre.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        textCentre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        textCentre.setText(": Loading :");
        textCentre.setFocusCycleRoot(true);

        textSub.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        textSub.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jButton1.setText("...");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(textCentre, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE)
            .addComponent(textSub, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(textCentre, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(textSub, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        switch(loadingState) // Check loading state in order to know what kind of loading is this and this button will understands what should it do
        {
            default: break;
            case 1: new ConfigEditor(); break;
            case 2: break; // Not yet implemented, for future developments
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    public void setButtonText(String s)
    {
        jButton1.setText(s);
    }
    
    public void setState(int state)
    {
        loadingState = state;
    }
    
    public void buttonShow(boolean bol)
    {
        jButton1.setVisible(bol);
    }
    
    public void enableClose(int i)
    {
        this.setDefaultCloseOperation(i);
    }
    
    public void setText(String s)
    {
        textCentre.setText(s);
    }
    
    public void setSub(String s)
    {
        textSub.setText(s);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel textCentre;
    private javax.swing.JLabel textSub;
    // End of variables declaration//GEN-END:variables
}
