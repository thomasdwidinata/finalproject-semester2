package magicstore.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.text.*;
import javax.swing.JOptionPane;
import magicstore.classes.DB;
import magicstore.classes.LoginCredentials;
import magicstore.classes.QueryProcessor;
import magicstore.classes.dataFill;

/* 
    EDITOR: LUFFANDRI, STEVEN
*/

public class CheckOut extends javax.swing.JFrame {

    public float total;
    float shipping=0;
    boolean cc=false; //WHETHER USER HAS CREDITCARD OR NOT
    String shippingtype;
    String paymenttype;
    int shipID;
    int payID;
    boolean purchased; //WHETHER PURCHASE WAS SUCCESSFULL OR NOT
    
    Date date = new Date( ); //FOR PAYMENT DATE
    SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
    
    private void closer() // TO REFRESH 'MyProfile' WINDOW
    {
        new MyProfile();
        this.dispose();
    }
    
    public CheckOut() {  //SET THE DEFAULT PAGE FOR CHECKOUT      
        initComponents();
        this.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
               closer();
            }
        });   
        
        ArrayList<String> alist = new ArrayList<String>();
        alist.add(String.valueOf(LoginCredentials.getID()));
        
        cartTable.setModel(dataFill.fillTable(5,alist));
        
        ccField.setEnabled(false);
        cvvField.setEnabled(false);
        ccMonth.setEnabled(false);
        ccYear.setEnabled(false);
        
        refresh();
    }
    
    public void refresh(){ //REFRESH THE PAGE AND INPUT IN THE CHECKOUT WINDOW
        
        total = 0;
        final int stockCol = 2;
        final int qtyCol = 5;
        for(int i = 0; i < cartTable.getRowCount(); ++i)
            total += (Float.valueOf(String.valueOf(cartTable.getValueAt(i, stockCol))) * Float.valueOf(String.valueOf(cartTable.getValueAt(i, qtyCol))));
        
        total += shipping;
        totalField.setText(String.valueOf(total));
        shippingField.setText(String.valueOf(shipping));
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        shippingGroup = new javax.swing.ButtonGroup();
        paymentGroup = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        cartTable = new javax.swing.JTable();
        CODbutton = new javax.swing.JRadioButton();
        CCbutton = new javax.swing.JRadioButton();
        CCpanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        nameField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        ccField = new javax.swing.JTextField();
        ccMonth = new javax.swing.JComboBox<>();
        ccYear = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        cvvField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        addressField = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        freeButton = new javax.swing.JRadioButton();
        expressButton = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        shippingField = new javax.swing.JLabel();
        totalField = new javax.swing.JLabel();
        purchaseButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Check Out");

        cartTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(cartTable);

        paymentGroup.add(CODbutton);
        CODbutton.setText("Cash on Delivery");
        CODbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CODbuttonActionPerformed(evt);
            }
        });

        paymentGroup.add(CCbutton);
        CCbutton.setText("Credit Card");
        CCbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CCbuttonActionPerformed(evt);
            }
        });

        jLabel2.setText("Name:");

        jLabel3.setText("Credit Card:");

        ccMonth.setMaximumRowCount(12);
        ccMonth.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
        ccMonth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ccMonthActionPerformed(evt);
            }
        });

        ccYear.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "16", "17", "18", "19", "20", "21", "22", "23" }));

        jLabel10.setText("/");

        jLabel9.setText("Address:");

        addressField.setColumns(20);
        addressField.setLineWrap(true);
        addressField.setRows(5);
        jScrollPane2.setViewportView(addressField);

        javax.swing.GroupLayout CCpanelLayout = new javax.swing.GroupLayout(CCpanel);
        CCpanel.setLayout(CCpanelLayout);
        CCpanelLayout.setHorizontalGroup(
            CCpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(CCpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CCpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CCpanelLayout.createSequentialGroup()
                        .addGroup(CCpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel9)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(CCpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ccField)
                            .addGroup(CCpanelLayout.createSequentialGroup()
                                .addGroup(CCpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                                    .addComponent(nameField, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(48, 48, 48))
                    .addGroup(CCpanelLayout.createSequentialGroup()
                        .addComponent(ccMonth, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ccYear, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cvvField, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(89, Short.MAX_VALUE))))
        );
        CCpanelLayout.setVerticalGroup(
            CCpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CCpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CCpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(CCpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CCpanelLayout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(0, 55, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(CCpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ccField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(CCpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ccYear, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ccMonth, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(cvvField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jLabel4.setText("Shipping Options");

        shippingGroup.add(freeButton);
        freeButton.setText("Free Shipping");
        freeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                freeButtonActionPerformed(evt);
            }
        });

        shippingGroup.add(expressButton);
        expressButton.setText("Express Shipping");
        expressButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                expressButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(freeButton)
                    .addComponent(expressButton)
                    .addComponent(jLabel4))
                .addContainerGap(9, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(freeButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(expressButton)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        jLabel5.setText("Shipping Cost:");

        jLabel6.setText("Rp.");

        jLabel7.setText("Total:");

        jLabel8.setText("Rp.");

        shippingField.setText("jLabel9");

        totalField.setText("jLabel11");

        purchaseButton.setText("Purchase");
        purchaseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purchaseButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(CCbutton)
                                    .addComponent(CODbutton)
                                    .addComponent(CCpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel7)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel8))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel5)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel6)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(shippingField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(totalField, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(purchaseButton)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CODbutton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(CCbutton)
                        .addGap(18, 18, 18)
                        .addComponent(CCpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(shippingField))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(totalField))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(purchaseButton)
                            .addComponent(cancelButton))
                        .addGap(45, 45, 45)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CODbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CODbuttonActionPerformed
        //IF USER PRESSED ON 'CASH ON DELIVERY', DISABLE CC FIELD
        
        ccField.setEnabled(false);
        cvvField.setEnabled(false);
        ccMonth.setEnabled(false);
        ccYear.setEnabled(false);
        
        cc=false;
        paymenttype="Cash on Delivery";
        payID=2;
    }//GEN-LAST:event_CODbuttonActionPerformed

    private void ccMonthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ccMonthActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ccMonthActionPerformed

    private void purchaseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purchaseButtonActionPerformed
        //VALIDATES THE INPUT CHECKING, AND CONFIRMING PURCHASE
        
        boolean valid = true;
        purchased=false;
        
        if(shippingGroup.getSelection()!=freeButton.getModel() && shippingGroup.getSelection()!=expressButton.getModel()){
            JOptionPane.showMessageDialog(null, "Please pick a shipping type!");
            valid=false;
        }
        if(paymentGroup.getSelection()!=CODbutton.getModel() && paymentGroup.getSelection()!=CCbutton.getModel()){
            JOptionPane.showMessageDialog(null, "Please pick a payment type!");
            valid=false;
        }
        if(nameField.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Please enter your name!");
            valid=false;
        }
        if(addressField.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Please enter your address!");
            valid=false;
        }
            
        
        if(valid){
            if(cc){ //IF USER HAS CC
                if(ccField.getText().length()==16 && cvvField.getText().length()==3){
                    int a = JOptionPane.showConfirmDialog(null, "Name: "+nameField.getText()+"\nPayment: "+paymenttype+"\nCredit Card: "+ccField.getText()+"\nShipping: "+shippingtype+"\nAddress: "+addressField.getText()+"\nGrand Total: Rp. "+total+"\n ", "Confirmation", JOptionPane.YES_NO_OPTION);
                    if(a==0){
                        try {
                            purchase();
                            purchased=true;
                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(null, "Unable to purchase!");
                        }
                        
                    }
                }
                else{
                    JOptionPane.showMessageDialog(null, "Invalid Credit Card!");
                }
            }
            else{ //IF USER DOES NOT HAVE CC
                int a = JOptionPane.showConfirmDialog(null, "Name: "+nameField.getText()+"\nPayment: "+paymenttype+"\nShipping: "+shippingtype+"\nAddress: "+addressField.getText()+"\nGrand Total: Rp. "+total+"\n ", "Confirmation", JOptionPane.YES_NO_OPTION);
                if(a==0){
                    try {
                        purchase();
                        purchased=true;
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Unable to purchase!");
                    }
                }
            }
        }
        if(purchased){ //AFTER PURCHASE
            JOptionPane.showMessageDialog(null, "You have successfully purchased your order!");
            this.dispose();
            new MyProfile();
        }
    }//GEN-LAST:event_purchaseButtonActionPerformed

    private void purchase() throws SQLException{
        // ALL THE QUERIES FOR THE PURCHASE, INPUTING INTO HISTORY,
        // DELETING FROM CART
        
        DB.executeUpdate("INSERT INTO purchase_history (user_id, payment_id, payment_date, total, shipping_id) VALUES ("+LoginCredentials.getID()+", "+payID+", '"+ft.format(date)+"', "+total+", "+shipID+")");
        
        ResultSet rs = DB.executeQuery("SELECT MAX(order_id) AS order_id FROM purchase_history");
        rs.last();
        
        int order_id = rs.getInt("order_id");
        
        int game_idCol=0;
        int qtyCol=5;
        
        int game_id;
        int qty;
        
        for(int i = 0; i < cartTable.getRowCount(); i++){
            game_id = Integer.valueOf(String.valueOf(cartTable.getValueAt(i, game_idCol)));
            qty = Integer.valueOf(String.valueOf(cartTable.getValueAt(i, qtyCol)));
            
            DB.executeUpdate("INSERT INTO game_purchase_history VALUES ("+order_id+", "+game_id+", "+qty+")");
        }
        
        ArrayList<String> alist = new ArrayList<String>();
        alist.add(String.valueOf(LoginCredentials.getID()));
        QueryProcessor.delete(2, alist);
    }
    
    
    private void expressButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_expressButtonActionPerformed
        // EXPRESS SHIPPING OPTION
        
        shipping = 25000;
        shippingtype="Express Shipping";
        shipID=1;
        refresh();
    }//GEN-LAST:event_expressButtonActionPerformed

    private void freeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_freeButtonActionPerformed
        //FREE SHIPPING OPTION
        
        shipping = 0;
        shippingtype="Free Shipping";
        shipID=2;
        refresh();
    }//GEN-LAST:event_freeButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        //CANCEL PURCHASE
        
        this.dispose();
        new MyProfile();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void CCbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CCbuttonActionPerformed
        //IF USER CHOOSES TO PAY BY CREDIT CARD
        //ENABLES CREDIT CARD FIELD
        
        ccField.setEnabled(true);
        cvvField.setEnabled(true);
        ccMonth.setEnabled(true);
        ccYear.setEnabled(true);
        
        cc=true;
        paymenttype="Credit Card";
        payID=1;
    }//GEN-LAST:event_CCbuttonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton CCbutton;
    private javax.swing.JPanel CCpanel;
    private javax.swing.JRadioButton CODbutton;
    private javax.swing.JTextArea addressField;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTable cartTable;
    private javax.swing.JTextField ccField;
    private javax.swing.JComboBox<String> ccMonth;
    private javax.swing.JComboBox<String> ccYear;
    private javax.swing.JTextField cvvField;
    private javax.swing.JRadioButton expressButton;
    private javax.swing.JRadioButton freeButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField nameField;
    private javax.swing.ButtonGroup paymentGroup;
    private javax.swing.JButton purchaseButton;
    private javax.swing.JLabel shippingField;
    private javax.swing.ButtonGroup shippingGroup;
    private javax.swing.JLabel totalField;
    // End of variables declaration//GEN-END:variables
}
