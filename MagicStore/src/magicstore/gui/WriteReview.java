/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magicstore.gui;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import magicstore.classes.DB;
import magicstore.classes.LoginCredentials;

/*
    EDITOR: STEVEN
*/

//FOR USER TO WRITE NEW REVIEW OR UPDATE PREVIOUS REVIEW

public class WriteReview extends javax.swing.JFrame {

    int gameid;
    int userid = LoginCredentials.getID();
    
    
    boolean reviewed = false; // WHETHER USER HAS REVIEWED THIS GAME OR NOT
    
    public WriteReview(int game_id) throws SQLException {
        initComponents();
        gameid = game_id;
        ResultSet rs = DB.executeQuery("SELECT * FROM review");
        while(rs.next())
        {
            int curGameid = rs.getInt("game_id");
            int curUserid = rs.getInt("user_id");
            if(curGameid==gameid && curUserid==userid){
                String review = rs.getString("review_content");
                reviewField.setText(review);
                reviewed=true;
                break;
            }
        }
        if(!reviewed){
            reviewField.setText("");
        }
        this.setVisible(true);
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        reviewPanel = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        reviewField = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        reviewPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Review", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 14))); // NOI18N

        jButton1.setText("Write Review");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        reviewField.setColumns(20);
        reviewField.setFont(new java.awt.Font("Nyala", 0, 18)); // NOI18N
        reviewField.setLineWrap(true);
        reviewField.setRows(5);
        reviewField.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jScrollPane2.setViewportView(reviewField);

        javax.swing.GroupLayout reviewPanelLayout = new javax.swing.GroupLayout(reviewPanel);
        reviewPanel.setLayout(reviewPanelLayout);
        reviewPanelLayout.setHorizontalGroup(
            reviewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(reviewPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(reviewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(reviewPanelLayout.createSequentialGroup()
                        .addGap(0, 270, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );
        reviewPanelLayout.setVerticalGroup(
            reviewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(reviewPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(reviewPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(reviewPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        String review = reviewField.getText();
        
        if(reviewed){
            DB.executeUpdate("UPDATE review SET review_content='"+review+"' WHERE game_id="+gameid+" AND user_id="+userid);
        }
        else{
            DB.executeUpdate("INSERT INTO review VALUES ('"+review+"', "+userid+", "+gameid+")");
        }
        
        JOptionPane.showMessageDialog(null, "Review has been updated!");
       this.dispose(); 
        
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea reviewField;
    private javax.swing.JPanel reviewPanel;
    // End of variables declaration//GEN-END:variables
}
