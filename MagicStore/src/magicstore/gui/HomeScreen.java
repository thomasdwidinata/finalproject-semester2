package magicstore.gui;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import magicstore.classes.DB;
import magicstore.classes.GUIController;
import magicstore.classes.LoginCredentials;
import magicstore.classes.MagicStore;
import magicstore.classes.ProgramConfig;
import magicstore.classes.QueryProcessor;
import magicstore.classes.dataFill;

/*
    EDITOR: THOMAS, STEVEN
*/

public class HomeScreen extends JFrame
{
    
    public HomeScreen()
    {
        initComponents();
        DB.disconnect();
        checkLoginStatus();
        this.setLocationRelativeTo(null);
        this.setTitle("Store Application");
        this.setVisible(true);
        fillData();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        logo = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        jPanel3 = new javax.swing.JPanel();
        nameBox = new javax.swing.JTextField();
        nameCheck = new javax.swing.JCheckBox();
        searchField = new javax.swing.JTextField();
        tablesToBeSort = new javax.swing.JComboBox<>();
        sortBox = new javax.swing.JComboBox<>();
        searchBox = new javax.swing.JComboBox<>();
        labelCheck = new javax.swing.JCheckBox();
        sortCheck = new javax.swing.JCheckBox();
        searchButton = new javax.swing.JButton();
        resetButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        gameTable = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        file_about = new javax.swing.JMenuItem();
        file_exit = new javax.swing.JMenuItem();
        profileNoLoginMenu = new javax.swing.JMenu();
        profile_login = new javax.swing.JMenuItem();
        profile_reg = new javax.swing.JMenuItem();
        profileLoginMenu = new javax.swing.JMenu();
        profile_my = new javax.swing.JMenuItem();
        editProfile = new javax.swing.JMenuItem();
        profile_purchases = new javax.swing.JMenuItem();
        profile_wishlist = new javax.swing.JMenuItem();
        profile_logout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/LollipopItem (2)(1).png"))); // NOI18N

        jLabel3.setFont(new java.awt.Font("Segoe UI", 2, 48)); // NOI18N
        jLabel3.setText("Magic Store");

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        nameBox.setEnabled(false);

        nameCheck.setText("Game Name : ");
        nameCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameCheckActionPerformed(evt);
            }
        });

        searchField.setEnabled(false);

        tablesToBeSort.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        tablesToBeSort.setEnabled(false);

        sortBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ascending", "Descending" }));
        sortBox.setEnabled(false);

        searchBox.setEnabled(false);

        labelCheck.setText("Search :");
        labelCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                labelCheckActionPerformed(evt);
            }
        });

        sortCheck.setText("Sort by : ");
        sortCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sortCheckActionPerformed(evt);
            }
        });

        searchButton.setText("Search");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });

        resetButton.setText("Reset");
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        gameTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        gameTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                gameTableMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                gameTableMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(gameTable);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(labelCheck)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(searchBox, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(searchField, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(nameCheck)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nameBox, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                        .addComponent(searchButton)
                        .addGap(18, 18, 18)
                        .addComponent(resetButton))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(sortCheck)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sortBox, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tablesToBeSort, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(searchBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(searchField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(nameCheck)
                        .addComponent(nameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(searchButton)
                        .addComponent(resetButton)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sortCheck)
                    .addComponent(sortBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tablesToBeSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 385, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLayeredPane1.setLayer(jPanel3, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        fileMenu.setText("File");

        file_about.setText("About");
        file_about.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                file_aboutActionPerformed(evt);
            }
        });
        fileMenu.add(file_about);

        file_exit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        file_exit.setText("Exit");
        file_exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                file_exitActionPerformed(evt);
            }
        });
        fileMenu.add(file_exit);

        jMenuBar1.add(fileMenu);

        profileNoLoginMenu.setText("Profile");

        profile_login.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        profile_login.setText("Login");
        profile_login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                profile_loginActionPerformed(evt);
            }
        });
        profileNoLoginMenu.add(profile_login);

        profile_reg.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
        profile_reg.setText("Register");
        profile_reg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                profile_regActionPerformed(evt);
            }
        });
        profileNoLoginMenu.add(profile_reg);

        jMenuBar1.add(profileNoLoginMenu);

        profileLoginMenu.setText("Profile");

        profile_my.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        profile_my.setText("My Profile");
        profile_my.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                profile_myActionPerformed(evt);
            }
        });
        profileLoginMenu.add(profile_my);

        editProfile.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        editProfile.setText("Edit Profile");
        editProfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editProfileActionPerformed(evt);
            }
        });
        profileLoginMenu.add(editProfile);

        profile_purchases.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        profile_purchases.setText("Cart");
        profile_purchases.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                profile_purchasesActionPerformed(evt);
            }
        });
        profileLoginMenu.add(profile_purchases);

        profile_wishlist.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_W, java.awt.event.InputEvent.CTRL_MASK));
        profile_wishlist.setText("Wish List");
        profile_wishlist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                profile_wishlistActionPerformed(evt);
            }
        });
        profileLoginMenu.add(profile_wishlist);

        profile_logout.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        profile_logout.setText("Logout");
        profile_logout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                profile_logoutActionPerformed(evt);
            }
        });
        profileLoginMenu.add(profile_logout);

        jMenuBar1.add(profileLoginMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(logo, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(344, 344, 344)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(logo, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
   
    private void fillData() //FILL TABLE WITH GAME LIST
    {
        gameTable.setModel(dataFill.fillTable(1));
        searchBox.setModel(dataFill.fillBox(ProgramConfig.getSearchableItem()));
        tablesToBeSort.setModel(dataFill.fillBox(1));
    }
    
    public void fillTable(ResultSet rs)
    {
        gameTable.setModel(dataFill.fillTable(rs));
    }
    
    private void file_exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_file_exitActionPerformed
        if(JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to exit?", "Exit Application", JOptionPane.OK_CANCEL_OPTION) == 0)
            System.exit(0);
    }//GEN-LAST:event_file_exitActionPerformed

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
        //FILTER SEARCH FOR THE GAMES
        
        int i = 1;
        ResultSet rs = null;
        ArrayList<String> tempAlist = new ArrayList<String>();
        tempAlist.clear();
        if(labelCheck.isSelected())
            i = 2;
        if(sortCheck.isSelected())
            i = i * 3;
        if(nameCheck.isSelected())
            i = i * 4;
        switch(i)
        {
            case 1:
                JOptionPane.showMessageDialog(rootPane, "Please select at least one category!", "Game Search", JOptionPane.INFORMATION_MESSAGE);
                break;
                
            case 2:
                tempAlist.add(String.valueOf(searchBox.getSelectedItem()));
                tempAlist.add(searchField.getText());
                rs = QueryProcessor.getCustom(2, tempAlist);
                break;
                
            case 3:
                tempAlist.add(String.valueOf(tablesToBeSort.getSelectedItem()));
                switch(String.valueOf(sortBox.getSelectedItem()))
                {
                    case "Ascending":
                        tempAlist.add("ASC");
                        break;
                    case "Descending":
                        tempAlist.add("DESC");
                        break;
                }
                
                rs = QueryProcessor.getCustom(3, tempAlist);
                break;
                
            case 4:
                tempAlist.add("Game Name");
                tempAlist.add(nameBox.getText());
                rs = QueryProcessor.getCustom(2, tempAlist);
                break;
                
            case (2*3):
                tempAlist.add(String.valueOf(searchBox.getSelectedItem()));
                tempAlist.add(searchField.getText());
                tempAlist.add(String.valueOf(tablesToBeSort.getSelectedItem()));
                switch(String.valueOf(sortBox.getSelectedItem()))
                {
                    case "Ascending":
                        tempAlist.add("ASC");
                        break;
                    case "Descending":
                        tempAlist.add("DESC");
                        break;
                }
                
                rs = QueryProcessor.getCustom(16, tempAlist);
                break;
                
            case (2*4):
                tempAlist.add(String.valueOf(searchBox.getSelectedItem()));
                tempAlist.add(searchField.getText());
                tempAlist.add("Game Name");
                tempAlist.add(nameBox.getText());
                rs = QueryProcessor.getCustom(15, tempAlist);
                break;
                
            case (3*4):
                tempAlist.add("Game Name");
                tempAlist.add(nameBox.getText());
                tempAlist.add(String.valueOf(tablesToBeSort.getSelectedItem()));
                switch(String.valueOf(sortBox.getSelectedItem()))
                {
                    case "Ascending":
                        tempAlist.add("ASC");
                        break;
                    case "Descending":
                        tempAlist.add("DESC");
                        break;
                }
                
                rs = QueryProcessor.getCustom(2, tempAlist);
                break;
                
            case (2*3*4):
                tempAlist.add("Game Name");
                tempAlist.add(nameBox.getText());
                tempAlist.add(String.valueOf(searchBox.getSelectedItem()));
                tempAlist.add(searchField.getText());
                tempAlist.add(String.valueOf(tablesToBeSort.getSelectedItem()));
                switch(String.valueOf(sortBox.getSelectedItem()))
                {
                    case "Ascending":
                        tempAlist.add("ASC");
                        break;
                    case "Descending":
                        tempAlist.add("DESC");
                        break;
                }
                
                rs = QueryProcessor.getCustom(17, tempAlist);
                break;
                
        }
        if(!(i==1))
            fillTable(rs);
    }//GEN-LAST:event_searchButtonActionPerformed

    private void file_aboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_file_aboutActionPerformed
        new About(MagicStore.getVersion());
    }//GEN-LAST:event_file_aboutActionPerformed

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
        fillData();
    }//GEN-LAST:event_resetButtonActionPerformed

    private void sortCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sortCheckActionPerformed
        //SORT THE GAME
        
        if(sortBox.isEnabled())
        {
            sortBox.setEnabled(false);
            tablesToBeSort.setEnabled(false);
        }
        else
        {
            sortBox.setEnabled(true);
            tablesToBeSort.setEnabled(true);
        }
    }//GEN-LAST:event_sortCheckActionPerformed

    private void nameCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameCheckActionPerformed
        //SEARCH GAME BY NAME
        
        if(nameBox.isEnabled())
        {
            nameBox.setEnabled(false);
        }
        else
        {
            nameBox.setEnabled(true);
        }
    }//GEN-LAST:event_nameCheckActionPerformed

    private void profile_loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_profile_loginActionPerformed
       //OPENS 'LOGIN' WINDOW
        
        new GUIController(this); // Allowing LoginForm() to modify this Frame
        new LoginForm();
        GUIController.setEnabled(false);
    }//GEN-LAST:event_profile_loginActionPerformed

    
    private void profile_logoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_profile_logoutActionPerformed
        //LOGS THE USER OUT
        
        LoginCredentials.dropLogin();
        checkLoginStatus();
        JOptionPane.showMessageDialog(rootPane, "You have successfuly logged out", "Account Manager", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_profile_logoutActionPerformed

    private void gameTableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_gameTableMousePressed
        //TELLS THE PROGRAM WHICH ROW THE USER IS POINTING AT
        
        new GUIController(this);
        int row = gameTable.getSelectedRow();
        if(evt.getClickCount() == 2)
        {
            try {
                new GameInfo(Integer.valueOf(String.valueOf(gameTable.getValueAt(row, 0))));
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(rootPane, "ex", "Table Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_gameTableMousePressed


    private void labelCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_labelCheckActionPerformed
        //SEARCH FIELD
        
        if(searchBox.isEnabled())
        {
            searchBox.setEnabled(false);
            searchField.setEnabled(false);
        }
        else
        {
            searchBox.setEnabled(true);
            searchField.setEnabled(true);
        }
    }//GEN-LAST:event_labelCheckActionPerformed

    private void profile_regActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_profile_regActionPerformed
        //OPENS 'REGISTRATION' WINDOW
        
        new GUIController(this); // Allowing LoginForm() to modify this Frame
        new Registration(1);
        GUIController.setEnabled(false);
    }//GEN-LAST:event_profile_regActionPerformed

    private void editProfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editProfileActionPerformed
        //OPENS 'ConfirmPassword' BEFORE GOINT TO 'EditProfile'
        
        new GUIController(this);
        try {
            new ConfirmPassword().setVisible(true);
            GUIController.setEnabled(false);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(rootPane, "Failed to initialise authenticator! Error : " + ex,"Login failed", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_editProfileActionPerformed

    private void profile_myActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_profile_myActionPerformed
        //OPENS USER'S PROFILE
        
        new GUIController(this); // Allowing LoginForm() to modify this Frame
        new MyProfile();
        GUIController.setEnabled(false);
    }//GEN-LAST:event_profile_myActionPerformed

    private void profile_purchasesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_profile_purchasesActionPerformed
        new GUIController(this); // Allowing LoginForm() to modify this Frame
        new MyProfile(1);
        GUIController.setEnabled(false);
    }//GEN-LAST:event_profile_purchasesActionPerformed

    private void profile_wishlistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_profile_wishlistActionPerformed
        //OPENS WISHLIST OF USER
        
        new GUIController(this); // Allowing LoginForm() to modify this Frame
        new MyProfile(2);
        GUIController.setEnabled(false);
    }//GEN-LAST:event_profile_wishlistActionPerformed

    private void gameTableMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_gameTableMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_gameTableMouseExited

      
    
    // START GetLoginCredentials IMPLEMENTATION
    public static boolean isLoginValid()
    {
        return LoginCredentials.isLoginValid();
    }
    // END GetLoginCredentials IMPLEMENTATION

    
    public static void checkLoginStatus()
    {
        if(isLoginValid())
        {
            profileLoginMenu.setVisible(true);
            profileNoLoginMenu.setVisible(false);
            jLabel1.setText("Hello, " + LoginCredentials.getLogin() + "!");
        }
        else
        {
            profileLoginMenu.setVisible(false);
            profileNoLoginMenu.setVisible(true);
            jLabel1.setText("");
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem editProfile;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenuItem file_about;
    private javax.swing.JMenuItem file_exit;
    private javax.swing.JTable gameTable;
    private static javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JCheckBox labelCheck;
    private javax.swing.JLabel logo;
    private javax.swing.JTextField nameBox;
    private javax.swing.JCheckBox nameCheck;
    private static javax.swing.JMenu profileLoginMenu;
    private static javax.swing.JMenu profileNoLoginMenu;
    private javax.swing.JMenuItem profile_login;
    private javax.swing.JMenuItem profile_logout;
    private javax.swing.JMenuItem profile_my;
    private javax.swing.JMenuItem profile_purchases;
    private javax.swing.JMenuItem profile_reg;
    private javax.swing.JMenuItem profile_wishlist;
    private javax.swing.JButton resetButton;
    private javax.swing.JComboBox<String> searchBox;
    private javax.swing.JButton searchButton;
    private javax.swing.JTextField searchField;
    private javax.swing.JComboBox<String> sortBox;
    private javax.swing.JCheckBox sortCheck;
    private javax.swing.JComboBox<String> tablesToBeSort;
    // End of variables declaration//GEN-END:variables
}
