package magicstore.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import magicstore.classes.DB;
import magicstore.classes.GUIController;
import magicstore.classes.LoginCredentials;
import magicstore.classes.QueryProcessor;
import magicstore.classes.dataFill;

/*
    EDITOR: THOMAS, STEVEN
*/

public class MyProfile extends javax.swing.JFrame {

    private String username, fullname, email, creditcard;
    
    public MyProfile(int tab){
        initComponents();
        refresher();
        this.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
               closer();
            }
        });
        
        Panes.setSelectedIndex(tab);
        this.setTitle("My Profile");
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
    
    private void closer()
    {
        GUIController.setEnabled(true);
        this.dispose();
    }
    
    public MyProfile(){
        initComponents();
        this.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
               closer();
            }
        });
        refresher();
        this.setTitle("My Profile");
        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }
    
    public void fillTable() 
    {
        //FILLS THE TABLE OF THE USER'S CART, WISHLIST, AND PURCHASE HISTORY
        
        ArrayList<String> alist = new ArrayList<String>();
        alist.add(String.valueOf(LoginCredentials.getID()));
        purchasesTable.setModel(dataFill.fillTable(4, alist));
        cartTable.setModel(dataFill.fillTable(5,alist));
        wishTable.setModel(dataFill.fillTable(6, alist));
        reviewTable.setModel(dataFill.fillTable(7, alist));
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panes = new javax.swing.JTabbedPane();
        profilePane = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        usernameLabel = new javax.swing.JLabel();
        fullnameLabel1 = new javax.swing.JLabel();
        emailLabel = new javax.swing.JLabel();
        creditcardLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        purchasesTable = new javax.swing.JTable();
        cartPane = new javax.swing.JPanel();
        checkoutButton = new javax.swing.JButton();
        clearcartButton = new javax.swing.JButton();
        fullnameLabel2 = new javax.swing.JLabel();
        removeButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        cartTable = new javax.swing.JTable();
        wishlistPane = new javax.swing.JPanel();
        fullnameLabel3 = new javax.swing.JLabel();
        clearwishButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        wishTable = new javax.swing.JTable();
        deleteWishSelected = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        reviewTable = new javax.swing.JTable();
        fullnameLabel4 = new javax.swing.JLabel();
        removeButton1 = new javax.swing.JButton();
        clearreviewButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        Panes.setToolTipText("");
        Panes.setName(""); // NOI18N

        jLabel2.setText("Username:");

        jLabel3.setText("Credit Card:");

        jLabel4.setText("E-mail:");

        jLabel5.setText("Full Name");

        usernameLabel.setText("jLabel6");

        fullnameLabel1.setText("jLabel7");

        emailLabel.setText("jLabel8");

        creditcardLabel.setText("jLabel9");

        jLabel1.setText("Purchase History");

        purchasesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(purchasesTable);

        javax.swing.GroupLayout profilePaneLayout = new javax.swing.GroupLayout(profilePane);
        profilePane.setLayout(profilePaneLayout);
        profilePaneLayout.setHorizontalGroup(
            profilePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(profilePaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(profilePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE)
                    .addGroup(profilePaneLayout.createSequentialGroup()
                        .addGroup(profilePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(profilePaneLayout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(creditcardLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE))
                            .addGroup(profilePaneLayout.createSequentialGroup()
                                .addGroup(profilePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(profilePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(usernameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                                    .addComponent(fullnameLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(emailLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        profilePaneLayout.setVerticalGroup(
            profilePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(profilePaneLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(profilePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(usernameLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(profilePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(fullnameLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(profilePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(emailLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(profilePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(creditcardLabel))
                .addGap(39, 39, 39)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
                .addContainerGap())
        );

        Panes.addTab("Profile", profilePane);

        checkoutButton.setText("Check Out");
        checkoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkoutButtonActionPerformed(evt);
            }
        });

        clearcartButton.setText("Clear Cart");
        clearcartButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearcartButtonActionPerformed(evt);
            }
        });

        fullnameLabel2.setText("full name");

        removeButton.setText("Remove Selected");
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButtonActionPerformed(evt);
            }
        });

        cartTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(cartTable);

        javax.swing.GroupLayout cartPaneLayout = new javax.swing.GroupLayout(cartPane);
        cartPane.setLayout(cartPaneLayout);
        cartPaneLayout.setHorizontalGroup(
            cartPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cartPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cartPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(cartPaneLayout.createSequentialGroup()
                        .addComponent(fullnameLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 147, Short.MAX_VALUE)
                        .addGroup(cartPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cartPaneLayout.createSequentialGroup()
                                .addComponent(removeButton)
                                .addGap(18, 18, 18)
                                .addComponent(clearcartButton))
                            .addComponent(checkoutButton, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        cartPaneLayout.setVerticalGroup(
            cartPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cartPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cartPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(cartPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(clearcartButton)
                        .addComponent(removeButton))
                    .addComponent(fullnameLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addComponent(checkoutButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 388, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9))
        );

        Panes.addTab("Cart", cartPane);

        fullnameLabel3.setText("full name");

        clearwishButton.setText("Clear Wishlist");
        clearwishButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearwishButtonActionPerformed(evt);
            }
        });

        wishTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        wishTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                wishTableMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(wishTable);

        deleteWishSelected.setText("Delete Selected");
        deleteWishSelected.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteWishSelectedActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout wishlistPaneLayout = new javax.swing.GroupLayout(wishlistPane);
        wishlistPane.setLayout(wishlistPaneLayout);
        wishlistPaneLayout.setHorizontalGroup(
            wishlistPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(wishlistPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(wishlistPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(wishlistPaneLayout.createSequentialGroup()
                        .addComponent(fullnameLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 135, Short.MAX_VALUE)
                        .addComponent(deleteWishSelected)
                        .addGap(18, 18, 18)
                        .addComponent(clearwishButton)))
                .addContainerGap())
        );
        wishlistPaneLayout.setVerticalGroup(
            wishlistPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, wishlistPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(wishlistPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fullnameLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(clearwishButton)
                    .addComponent(deleteWishSelected))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE)
                .addContainerGap())
        );

        Panes.addTab("Wishlist", wishlistPane);

        reviewTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(reviewTable);

        fullnameLabel4.setText("full name");

        removeButton1.setText("Remove Selected");
        removeButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButton1ActionPerformed(evt);
            }
        });

        clearreviewButton1.setText("Clear Review");
        clearreviewButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearreviewButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(fullnameLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 130, Short.MAX_VALUE)
                        .addComponent(removeButton1)
                        .addGap(18, 18, 18)
                        .addComponent(clearreviewButton1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(clearreviewButton1)
                        .addComponent(removeButton1))
                    .addComponent(fullnameLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE)
                .addContainerGap())
        );

        Panes.addTab("Reviews", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(Panes)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panes)
        );

        Panes.getAccessibleContext().setAccessibleName("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void clearcartButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearcartButtonActionPerformed
        //CLEARS THE USER'S CART
        
        int a = JOptionPane.showConfirmDialog(null, "Are you sure to delete all games on the cart?", "Confirmation", JOptionPane.YES_NO_OPTION);
        
        if(a==0){
            process(2);
            JOptionPane.showMessageDialog(rootPane, "Cart has been cleared", "Clear Cart", JOptionPane.INFORMATION_MESSAGE);
            
            int rowCount = cartTable.getRowCount();
            int game_id;
            int qty;
            int game_idCol=0;
            int qtyCol=5;
            
            for(int i=0; i<rowCount; ++i){
                game_id = Integer.valueOf(String.valueOf(cartTable.getValueAt(i, game_idCol)));
                qty = Integer.valueOf(String.valueOf(cartTable.getValueAt(i, qtyCol)));
                
                ResultSet rs = DB.executeQuery("SELECT * FROM game WHERE game_id="+game_id);
                try {
                    rs.last();
                    int stock = rs.getInt("stock");
                    stock += qty;
                    
                    DB.executeUpdate("UPDATE game SET stock="+stock+" WHERE game_id="+game_id);
                } catch (SQLException ex) {
                    JOptionPane.showConfirmDialog(rootPane, "Could not connect to database!");
                }
            }
            
            
            
        }
        refresher();
    }//GEN-LAST:event_clearcartButtonActionPerformed

    private void process(int method)
    {
        ArrayList<String> alist = new ArrayList<String>();
        alist.add(String.valueOf(LoginCredentials.getID()));
        switch(method)
        {
            case 1: QueryProcessor.delete(1, alist); break;
            case 2: QueryProcessor.delete(2, alist); break;
            case 3: QueryProcessor.delete(5, alist); break;
        }
        
    }
    
    public void refresher() //REFRESHES THE USER'S TABLES AFTER DOING SOMETHING
    {
        username = LoginCredentials.getLogin();
        
        ResultSet rs = DB.executeQuery("SELECT * FROM user WHERE username='"+username+"'");
        
        try {
            rs.last();
            
            fullname = rs.getString("fullname");
            email = rs.getString("email");
            creditcard = rs.getString("creditcard");
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Could not retrieve information!");
        }
        
        fullnameLabel1.setText(fullname);
        fullnameLabel2.setText(fullname);
        fullnameLabel3.setText(fullname);
        fullnameLabel4.setText(fullname);
        
        usernameLabel.setText(username);
        emailLabel.setText(email);
        creditcardLabel.setText(creditcard);
        
        fillTable();
    }
    
    private void clearwishButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearwishButtonActionPerformed
        //CLEAR THE WISHLIST TABLE
        
        if(JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to remove ALL games from the wishlist?", "Game removal", JOptionPane.YES_NO_OPTION) == 0)
        {
            process(1);
            JOptionPane.showMessageDialog(rootPane, "All games have been removed from the wishlist!", "Game removal", JOptionPane.INFORMATION_MESSAGE);
        }
        else
            JOptionPane.showMessageDialog(rootPane, "Game removal Aborted!", "Game removal", JOptionPane.INFORMATION_MESSAGE);
        refresher();
    }//GEN-LAST:event_clearwishButtonActionPerformed

    private void clearreviewButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearreviewButton1ActionPerformed
        //CLEAR ALL REVIEWS WRITTEN BY THE USER
        
        if(JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to remove ALL reviews made by you?", "Clear Reviews", JOptionPane.YES_NO_OPTION) == 0)
        {
            int tableCol=reviewTable.getRowCount();
            int gameid;
            for(int i=0; i<tableCol; i++){
                gameid = Integer.valueOf(String.valueOf(reviewTable.getValueAt(i, 2)));
                DB.executeUpdate("DELETE FROM review WHERE user_id="+LoginCredentials.getID()+" AND game_id="+gameid);
            }
            refresher();
            JOptionPane.showMessageDialog(rootPane, "All reviews have been deleted!", "Clear Reviews", JOptionPane.INFORMATION_MESSAGE);
        }
        else
            JOptionPane.showMessageDialog(rootPane, "Clear Aborted!", "Clear Reviews", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_clearreviewButton1ActionPerformed

    private void removeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButtonActionPerformed
        //REMOVES A SELECTED GAME FROM CART
        
        int row = cartTable.getSelectedRow();
        if(row < 0)
        {
            JOptionPane.showMessageDialog(rootPane, "Please select one game that is going to be removed", "Game removal", JOptionPane.INFORMATION_MESSAGE);
        }
        else
        {
            if(JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to remove this game from cart?", "Game removal", JOptionPane.YES_NO_OPTION) == 0)
            {
                ArrayList<String> alist = new ArrayList<String>();
                alist.add(String.valueOf(LoginCredentials.getID()));
                alist.add(String.valueOf(cartTable.getValueAt(row, 0)));
                QueryProcessor.delete(3, alist);
                JOptionPane.showMessageDialog(rootPane, "Game has been removed from the Cart!", "Game removal", JOptionPane.INFORMATION_MESSAGE);
                
                int game_id;
                int qty;
                
                int game_idCol=0;
                int qtyCol=5;
                
                game_id = Integer.valueOf(String.valueOf(cartTable.getValueAt(row, game_idCol)));
                qty = Integer.valueOf(String.valueOf(cartTable.getValueAt(row, qtyCol)));
                
                ResultSet rs = DB.executeQuery("SELECT * FROM game WHERE game_id="+game_id);
                try {
                    rs.last();
                    int stock = rs.getInt("stock");
                    stock += qty;
                    
                    DB.executeUpdate("UPDATE game SET stock="+stock+" WHERE game_id="+game_id);
                    
                } catch (SQLException ex) {
                    JOptionPane.showConfirmDialog(rootPane, "Could not connect to database!");
                }
            }
            else
            {
                JOptionPane.showMessageDialog(rootPane, "Game removal Aborted!", "Game removal", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        refresher();
    }//GEN-LAST:event_removeButtonActionPerformed

    private void checkoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkoutButtonActionPerformed
        //GOES TO CHECKOUT WINDOW
        
        if(cartTable.getRowCount() < 1)
            JOptionPane.showMessageDialog(rootPane, "No items to buy. Add some to checkout!", "Checkout Process", JOptionPane.INFORMATION_MESSAGE);
        else
        {
            new CheckOut().setVisible(true);
            refresher();
            this.dispose();
        }
    }//GEN-LAST:event_checkoutButtonActionPerformed

    private void deleteWishSelectedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteWishSelectedActionPerformed
        //DELETES SELECTED GAME FROM WISHLIST
        
        int row = wishTable.getSelectedRow();
        if(row < 0)
        {
            JOptionPane.showMessageDialog(rootPane, "Please select one game that is going to be removed", "Game removal", JOptionPane.INFORMATION_MESSAGE);
        }
        else
        {
            if(JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to remove this game from wishlist?", "Game removal", JOptionPane.YES_NO_OPTION) == 0)
            {
                ArrayList<String> alist = new ArrayList<String>();
                alist.add(String.valueOf(LoginCredentials.getID()));
                alist.add(String.valueOf(wishTable.getValueAt(row, 0)));
                
                QueryProcessor.delete(4, alist);
                JOptionPane.showMessageDialog(rootPane, "Game has been removed from the wishlist!", "Game removal", JOptionPane.INFORMATION_MESSAGE);
                
            }
            else
            {
                JOptionPane.showMessageDialog(rootPane, "Game removal Aborted!", "Game removal", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        refresher();
    }//GEN-LAST:event_deleteWishSelectedActionPerformed

    private void removeButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButton1ActionPerformed
        //REMOVES SELECTED GAME FROM WISHLIST
        
        int row = reviewTable.getSelectedRow();
        if(row < 0)
        {
            JOptionPane.showMessageDialog(rootPane, "Please select one game that is going to be removed", "Game removal", JOptionPane.INFORMATION_MESSAGE);
        }
        else
        {
            if(JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to remove this game from wishlist?", "Game removal", JOptionPane.YES_NO_OPTION) == 0)
            {
                ArrayList<String> alist = new ArrayList<String>();
                alist.add(String.valueOf(LoginCredentials.getID()));
                alist.add(String.valueOf(reviewTable.getValueAt(row, 2)));
                QueryProcessor.delete(6, alist);
                JOptionPane.showMessageDialog(rootPane, "Game has been removed from the wishlist!", "Game removal", JOptionPane.INFORMATION_MESSAGE);
                
            }
            else
            {
                JOptionPane.showMessageDialog(rootPane, "Game removal Aborted!", "Game removal", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        refresher();
    }//GEN-LAST:event_removeButton1ActionPerformed

    private void wishTableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_wishTableMousePressed
        int row = wishTable.getSelectedRow();
        try
        {
            if(evt.getClickCount() == 2)
            {
                new GameInfo(Integer.valueOf(String.valueOf(wishTable.getValueAt(row, 0))));
            }
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(cartPane, "Unable to show game! General Error: " + ex, "Show Game", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_wishTableMousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane Panes;
    private javax.swing.JPanel cartPane;
    private javax.swing.JTable cartTable;
    private javax.swing.JButton checkoutButton;
    private javax.swing.JButton clearcartButton;
    private javax.swing.JButton clearreviewButton1;
    private javax.swing.JButton clearwishButton;
    private javax.swing.JLabel creditcardLabel;
    private javax.swing.JButton deleteWishSelected;
    private javax.swing.JLabel emailLabel;
    private javax.swing.JLabel fullnameLabel1;
    private javax.swing.JLabel fullnameLabel2;
    private javax.swing.JLabel fullnameLabel3;
    private javax.swing.JLabel fullnameLabel4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JPanel profilePane;
    private javax.swing.JTable purchasesTable;
    private javax.swing.JButton removeButton;
    private javax.swing.JButton removeButton1;
    private javax.swing.JTable reviewTable;
    private javax.swing.JLabel usernameLabel;
    private javax.swing.JTable wishTable;
    private javax.swing.JPanel wishlistPane;
    // End of variables declaration//GEN-END:variables
}
