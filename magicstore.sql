-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2016 at 08:04 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `magicstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `user_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `game_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `qty` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`user_id`, `game_id`, `qty`) VALUES
(005, 002, 3);

-- --------------------------------------------------------

--
-- Table structure for table `developer`
--

CREATE TABLE `developer` (
  `developer_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `developer_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `developer`
--

INSERT INTO `developer` (`developer_id`, `developer_name`) VALUES
(001, 'mojang'),
(002, 'Oracle'),
(003, 'Valve'),
(004, 'Rockstar'),
(005, 'Ubisoft');

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `game_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `game_name` varchar(60) NOT NULL,
  `price` float NOT NULL,
  `stock` int(10) UNSIGNED NOT NULL,
  `developer_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `publisher_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `platform_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `tag_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `description` text NOT NULL,
  `release_date` date NOT NULL,
  `game_cover` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`game_id`, `game_name`, `price`, `stock`, `developer_id`, `publisher_id`, `platform_id`, `tag_id`, `description`, `release_date`, `game_cover`) VALUES
(001, 'Minecraft', 100000, 15, 001, 001, 001, 001, 'Minecraft is a game about placing blocks and going on adventures. Explore randomly generated worlds and build amazing things from the simplest of homes to the grandest of castles. Play in Creative Mode with unlimited resources or mine deep in Survival Mode, crafting weapons and armor to fend off dangerous mobs. Do all this alone or with friends.', '2016-05-03', NULL),
(002, 'Minecraft Special', 90000, 15, 001, 001, 003, 002, 'Minecraft is a game about placing blocks and going on adventures. Explore randomly generated worlds and build amazing things from the simplest of homes to the grandest of castles. Play in Creative Mode with unlimited resources or mine deep in Survival Mode, crafting weapons and armor to fend off dangerous mobs. Do all this alone or with friends.\r\n\r\nIn this special version, all the DLCs are included which unlocks a lot of special features in Minecraft.', '2016-04-13', NULL),
(003, 'Mojang Specials', 10200, 15, 001, 001, 004, 001, 'Mojang Specials is a complete pack that includes all the games developed by Mojang.', '2016-05-01', NULL),
(004, 'Java Adventurer', 150000, 15, 002, 002, 001, 001, 'Experience a new style of gaming where you become a programmer that excels in Java. You can create your own ideas for your application and become world famous!', '2016-06-02', NULL),
(005, 'Java Quiz Machine', 155000, 9, 002, 002, 001, 002, 'This game will test your ability in speaking in Java Programming Language. A very fun game for kids and teaches your kids to learn the easiest, fastest, and secure programming language, Java!', '2016-06-07', NULL),
(006, 'Minecraft Oracle Edition', 240000, 15, 002, 002, 004, 002, 'Minecraft is now part of Oracle! Thanks to Java, Minecraft is fun and cross platform!', '2016-06-10', NULL),
(007, 'Grand Theft Auto 5', 600000, 15, 004, 004, 001, 004, 'Play in an open world while being omnipotent.', '2016-06-15', NULL),
(008, 'Warcraft', 200000, 13, 003, 003, 001, 003, 'Play in a fantasy world ridden with cruel monsters. Be the hero and save the world!', '2015-01-14', NULL),
(009, 'FIFA 2016', 150000, 15, 005, 005, 003, 004, 'Can you lead your team to Victory? Assemble your own team and reach the World Cup!', '2016-02-01', NULL),
(010, 'Left 4 Dead 2', 140000, 25, 003, 003, 001, 002, 'Trapped in a zombie apocalypse.. Can you survive with the remaining survivors?', '2013-04-09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `game_platform`
--

CREATE TABLE `game_platform` (
  `game_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `platform_id` int(3) UNSIGNED ZEROFILL NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `game_platform`
--

INSERT INTO `game_platform` (`game_id`, `platform_id`) VALUES
(001, 001),
(002, 001),
(002, 002),
(003, 001),
(004, 001);

-- --------------------------------------------------------

--
-- Table structure for table `game_purchase_history`
--

CREATE TABLE `game_purchase_history` (
  `order_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `game_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game_purchase_history`
--

INSERT INTO `game_purchase_history` (`order_id`, `game_id`, `qty`) VALUES
(001, 001, 5),
(001, 003, 10),
(001, 005, 5),
(002, 002, 2),
(003, 005, 2),
(004, 005, 2),
(005, 005, 2),
(006, 005, 3),
(007, 005, 3),
(008, 008, 2);

-- --------------------------------------------------------

--
-- Table structure for table `game_tag`
--

CREATE TABLE `game_tag` (
  `game_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `game_tag` int(3) UNSIGNED ZEROFILL NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `payment_type` varchar(25) NOT NULL,
  `payment_id` int(3) UNSIGNED ZEROFILL NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`payment_type`, `payment_id`) VALUES
('Credit Card', 001),
('Cash on Delivery', 002);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(1) NOT NULL,
  `permission_level` varchar(6) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `permission_level`) VALUES
(1, 'user'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `platform_list`
--

CREATE TABLE `platform_list` (
  `platform_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `platform_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `platform_list`
--

INSERT INTO `platform_list` (`platform_id`, `platform_name`) VALUES
(001, 'Windows 10'),
(002, 'Nintendo Wii'),
(003, 'Playstation 4'),
(004, 'Xbox 1');

-- --------------------------------------------------------

--
-- Table structure for table `publisher`
--

CREATE TABLE `publisher` (
  `publisher_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `publisher_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `publisher`
--

INSERT INTO `publisher` (`publisher_id`, `publisher_name`) VALUES
(001, 'mojang publishing'),
(002, 'Oracle'),
(003, 'Valve'),
(004, 'Rockstar'),
(005, 'Ubisoft');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_history`
--

CREATE TABLE `purchase_history` (
  `order_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `user_id` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `payment_id` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `payment_date` date NOT NULL,
  `total` float NOT NULL,
  `shipping_id` int(3) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_history`
--

INSERT INTO `purchase_history` (`order_id`, `user_id`, `payment_id`, `payment_date`, `total`, `shipping_id`) VALUES
(001, 002, 001, '2016-06-12', 1402000, 001),
(002, 002, 002, '2016-06-12', 205000, 001),
(003, 002, 002, '2016-06-12', 335000, 001),
(004, 002, 002, '2016-06-12', 335000, 001),
(005, 006, 002, '2016-06-12', 335000, 001),
(006, 007, 002, '2016-06-13', 490000, 001),
(007, 008, 001, '2016-06-13', 490000, 001),
(008, 008, 002, '2016-06-13', 425000, 001);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `review_content` text NOT NULL,
  `user_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `game_id` int(3) UNSIGNED ZEROFILL NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`review_content`, `user_id`, `game_id`) VALUES
('I really like this game! I rec. to all my friends.sssss', 002, 002),
('I love this game so much... best day of my life', 002, 001),
('10/10 game', 005, 001),
('Game needs update!', 002, 004),
('I love this game\nso much', 002, 003),
('Best game ever in my life!', 007, 005);

-- --------------------------------------------------------

--
-- Table structure for table `shipping`
--

CREATE TABLE `shipping` (
  `shipping_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `shipping_mode` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shipping`
--

INSERT INTO `shipping` (`shipping_id`, `shipping_mode`) VALUES
(001, 'Express Shipping (1 Day)'),
(002, 'Normal Shipping');

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `tag_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `tag_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`tag_id`, `tag_name`) VALUES
(001, 'RPG'),
(002, 'Action'),
(003, 'Adventure'),
(004, 'Open World');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `username` varchar(16) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(45) NOT NULL,
  `creditcard` varchar(16) DEFAULT NULL,
  `profilepicture` text,
  `permission_group` int(1) NOT NULL,
  `fullname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `email`, `creditcard`, `profilepicture`, `permission_group`, `fullname`) VALUES
(001, 'thomas', 'thomas', 'thomas_dwidinata@hotmail.com', '', '', 2, ''),
(002, 'user', 'user', 'user@user.com', '9098829382738291', '', 1, 'User Full Name'),
(005, 'stevenmuliamin', 'password', 'steven.muliamin@yahoo.com', '4902918275627184', NULL, 1, 'Steven Muliamin'),
(006, 'frankiw', 'password', 'frankiw@yahoo.com', '2020101029292838', NULL, 1, 'Franki'),
(007, 'Lukas', 'password', 'lukas@gmail.com', NULL, NULL, 1, 'Lukas Graham'),
(008, 'Adam', 'password', 'adam@yahoo.com', '4029928192039281', NULL, 1, 'Adam Lucifer');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `user_id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `game_id` int(3) UNSIGNED ZEROFILL NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `game_id` (`game_id`);

--
-- Indexes for table `developer`
--
ALTER TABLE `developer`
  ADD PRIMARY KEY (`developer_id`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`game_id`),
  ADD UNIQUE KEY `game_name` (`game_name`),
  ADD KEY `developer_id` (`developer_id`),
  ADD KEY `publisher_id` (`publisher_id`),
  ADD KEY `tag_id` (`tag_id`),
  ADD KEY `platform_id` (`platform_id`);

--
-- Indexes for table `game_platform`
--
ALTER TABLE `game_platform`
  ADD KEY `game_id` (`game_id`),
  ADD KEY `platform_id` (`platform_id`);

--
-- Indexes for table `game_purchase_history`
--
ALTER TABLE `game_purchase_history`
  ADD KEY `order_id` (`order_id`),
  ADD KEY `game_id` (`game_id`);

--
-- Indexes for table `game_tag`
--
ALTER TABLE `game_tag`
  ADD KEY `game_id` (`game_id`),
  ADD KEY `game_tag` (`game_tag`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `platform_list`
--
ALTER TABLE `platform_list`
  ADD PRIMARY KEY (`platform_id`);

--
-- Indexes for table `publisher`
--
ALTER TABLE `publisher`
  ADD PRIMARY KEY (`publisher_id`);

--
-- Indexes for table `purchase_history`
--
ALTER TABLE `purchase_history`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `payment_id` (`payment_id`),
  ADD KEY `shipping_id` (`shipping_id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `game_id` (`game_id`);

--
-- Indexes for table `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`shipping_id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `permission_group` (`permission_group`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `game_id` (`game_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `developer`
--
ALTER TABLE `developer`
  MODIFY `developer_id` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `game_id` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `platform_list`
--
ALTER TABLE `platform_list`
  MODIFY `platform_id` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `publisher`
--
ALTER TABLE `publisher`
  MODIFY `publisher_id` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `purchase_history`
--
ALTER TABLE `purchase_history`
  MODIFY `order_id` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `shipping`
--
ALTER TABLE `shipping`
  MODIFY `shipping_id` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `tag_id` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `game_ibfk_1` FOREIGN KEY (`developer_id`) REFERENCES `developer` (`developer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `game_ibfk_2` FOREIGN KEY (`publisher_id`) REFERENCES `publisher` (`publisher_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `game_ibfk_3` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `game_ibfk_4` FOREIGN KEY (`platform_id`) REFERENCES `platform_list` (`platform_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `game_platform`
--
ALTER TABLE `game_platform`
  ADD CONSTRAINT `game_platform_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `game_platform_ibfk_2` FOREIGN KEY (`platform_id`) REFERENCES `platform_list` (`platform_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `game_purchase_history`
--
ALTER TABLE `game_purchase_history`
  ADD CONSTRAINT `game_purchase_history_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `purchase_history` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `game_purchase_history_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `game_tag`
--
ALTER TABLE `game_tag`
  ADD CONSTRAINT `game_tag_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `game_tag_ibfk_2` FOREIGN KEY (`game_tag`) REFERENCES `tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purchase_history`
--
ALTER TABLE `purchase_history`
  ADD CONSTRAINT `purchase_history_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_history_ibfk_2` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`payment_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_history_ibfk_3` FOREIGN KEY (`shipping_id`) REFERENCES `shipping` (`shipping_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `permission_group` FOREIGN KEY (`permission_group`) REFERENCES `permission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wishlist_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
